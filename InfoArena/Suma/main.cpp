#include <fstream>
using namespace std;

int main() {
    ifstream fin("suma.in");
    ofstream fout("suma.out");
    int n, s = 0, p;
    fin >> n >> p;

    for (int i = 1; i <= n; ++i) {
        s += i * (i - 1);
    }

    fout << s % p;
    return 0;
}