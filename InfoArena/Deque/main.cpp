#include <fstream>
#include <deque>
#define ll long long
using namespace std;

ifstream fin("deque.in");
ofstream fout("deque.out");
int nums[5000005];

int main() {
    deque<int> q;
    int n, k;
    ll sum = 0;
    fin >> n >> k;

    for (int i = 1; i <= n; ++i)
        fin >> nums[i];

    for (int i = 1; i <= n; ++i) {
        while (!q.empty() && nums[i] <= nums[q.back()])
            q.pop_back();
        q.emplace_back(i);

        if (q.front() == i - k)
            q.pop_front();
        if (i >= k)
            sum += nums[q.front()];
    }

    fout << sum;
    return 0;
}