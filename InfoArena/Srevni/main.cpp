#include <fstream>
#include <vector>
using namespace std;

vector <int> muchii[100005];
int preturi[100005];
bool vizitat[100005];
int rezultate[100005];

void find(int nod, int &minim) {
    for (unsigned int i = 0; i < muchii[nod].size(); ++i) {
        int vecin = muchii[nod][i];

        if (rezultate[vecin] > 0) {
            if (rezultate[vecin] < minim)
                minim = rezultate[vecin];
            continue;
        }

        if (preturi[vecin] < minim)
            minim = preturi[vecin];

        int temp = minim;

        if (!vizitat[vecin]) {
            vizitat[vecin] = true;
            find(vecin, minim);
        }

        if (minim < temp)
            rezultate[vecin] = minim;

        vizitat[vecin] = false;
    }
}

int main() {
    ifstream fin("srevni.in");
    ofstream fout("srevni.out");

    int n, m;
    fin >> n >> m;

    for (int i = 1; i <= n; ++i)
        fin >> preturi[i];

    for (int i = 1; i <= m; ++i) {
        int x, y;
        fin >> x >> y;
        muchii[x].push_back(y);
    }

    for (int i = 1; i <= n; ++i) {
        if (rezultate[i] > 0) {
            fout << rezultate[i] << " ";
            continue;
        }

        int minim = preturi[i];
        find(i, minim);
        fout << minim << " ";
    }

    return 0;
}
