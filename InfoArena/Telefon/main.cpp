#include <fstream>
#include <iostream>
using namespace std;

void inversNr (int &x) {
    int invers = 0, ultimaCifra;
    while (x > 0) {
        ultimaCifra = x % 10;
        x /= 10;
        invers = (invers * 10) + ultimaCifra;
    }
    x = invers;
}

int main() {
    ifstream fin("date.in");
    ofstream fout("date.out");
    int tastatura_sus[3] = {1, 2, 3}, tastatura_mij[3] = {4, 5, 6}, tastatura_jos[3] = {7, 8, 9}, tastatura_zero[3] = {11, 0, -1};
    int pos = tastatura_jos[0], pos_tast = 3;
    fout << "S ";
    int tinta, contor = 0;
    char numar_ch[11];
    fin.get(numar_ch, 11);

    do {
        tinta = numar_ch[contor];
        bool gasit = false;
        ++contor;
        
        for (int i = 0; i < 3; ++i) {
            if (pos_tast == 1) {
                if (tastatura_sus[i] == tinta) {
                    gasit = true;
                    break;
                }
            } else if (pos_tast == 2) {
                if (tastatura_mij[i] == tinta) {
                    gasit = true;
                    break;
                }
            } else if (pos_tast == 3) {
                if (tastatura_jos[i] == tinta) {
                    gasit == true;
                    break;
                }
            } 
            
        }
        if (gasit) {
            if (tinta == pos - 2) {
                fout << "ST 2 A ";
                pos -= 2;
                continue;
            } else if (tinta == pos - 1) {
                fout << "ST 1 A ";
                --pos;
                continue;
            } else if (tinta == pos + 2) {
                fout << "DR 2 A ";
                pos += 2;
                continue;
            } else if (tinta == pos + 1) {
                fout << "DR 1 A ";
                --pos;
                continue;
            } else {
                fout << "A ";
            }
        }

        
        
    } while (contor < 10); 
}