#include <fstream>
#include <bitset>
using namespace std;

ifstream fin("permutari.in");
ofstream fout("permutari.out");

int n, perm[10];
bitset<10> used;

void genPerm(int level) {
    if (level > n) {
        for (int i = 1; i <= n; ++i)
            fout << perm[i] << " ";
        fout << "\n";
    } else {
        for (int i = 1; i <= n; ++i)
            if (!used[i]) {
                used[i] = true;
                perm[level] = i;
                genPerm(level + 1);
                used[i] = false;
            }
    }
}

int main() {
    fin >> n;
    genPerm(1);
    return 0;
}