#include <fstream>
#define ll long long
using namespace std;

ifstream fin("lgput.in");
ofstream fout("lgput.out");

int main() {
    ll n, p, result = 1, m = 1999999973;
    fin >> n >> p;

    for (ll i = 0; (1 << i) <= p; ++i) {
        if (((1 << i) & p) > 0)
            result = (result * n) % m;
        n = (n * n) % m;
    }

    fout << result;
    return 0;
}