#include <fstream>
#include <stack>
#include <vector>
using namespace std;

vector <int> muchii[100005];
bool vizitat[100005];
stack <int> sortaret;

void find(int nod) {
    if(vizitat[nod])
        return;
    else
        vizitat[nod] = true;

    for (unsigned int i = 0; i < muchii[nod].size(); ++i)
        find(muchii[nod][i]);

    sortaret.push(nod);
}

int main() {
    ifstream fin("sortaret.in");
    ofstream fout("sortaret.out");

    int n, m;
    fin >> n >> m;
    for (int i = 1; i <= m; ++i) {
        int x, y;
        fin >> x >> y;
        muchii[x].push_back(y);
    }

    for (int i = 1; i <= n; ++i)
        find(i);

    while (!sortaret.empty()) {
        fout << sortaret.top() << " ";
        sortaret.pop();
    }
}
