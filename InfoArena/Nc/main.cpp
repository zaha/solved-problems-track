#include <fstream>
#include <iostream>
using namespace std;

bool esteLitera (char caracter) {
    if (('a' <= caracter && caracter <= 'z') || ('A' <= caracter && caracter <= 'Z')) {
        return true;
    } else {
        return false;
    }
}

bool sfarsitDeFraza (char caracter) {
    if (caracter == '.' || caracter == '?' || caracter == '!') {
        return true;
    } else {
        return false;
    }
}

int main() {
    char caracter;
    int contor_cuv = 0, contor_fraze = 0, lista_cuvinte[4003];
    bool cuvant = false, fraza = false, punct;

    ifstream fin("nc.in");
    ofstream fout("nc.out");

    while (fin >> caracter) {
        cout << caracter;
        if (punct) {
            if (caracter == '.') {
                continue;
            } else {
                punct = false;
            }
        }

        if (esteLitera) {
            if (!cuvant) {
                ++contor_cuv;
                cuvant = true;
            }
            if (!fraza) {
                ++contor_fraze;
                fraza = true;
            }
        } else {
            if (cuvant) {
                cuvant = false;
            }
            if (sfarsitDeFraza) {
                if (caracter == '.') {
                    punct = true;
                }
                contor_cuv = 0;
                fraza = false;
                lista_cuvinte[contor_fraze] = contor_cuv;
            }
        }
    }

    fout << contor_fraze << "\n";
    for (int i = 1; i <= contor_fraze; ++i) {
        fout << lista_cuvinte[i] << "\n";
    }
}