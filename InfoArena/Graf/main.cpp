#include <bits/stdc++.h>
using namespace std;

ifstream fin("graf.in");
ofstream fout("graf.out");

const int N = 14e3 + 7;
vector<int> edges[N];
int dist_x[N], dist_y[N]; //distance from node i to node x, y
int cnt[N]; //how many times distance i appears in dist_x

void calc_dist(int root, int *dist) {
//bfs to calc distance from root to other nodes

    queue<int> to_visit;
    bitset<N> added;
    to_visit.emplace(root);
    added[root] = true;
    dist[root] = 0;

    while (!to_visit.empty()) {
        int node = to_visit.front();
        to_visit.pop();

        for (int i : edges[node])
            if (!added[i]) {
                dist[i] = dist[node] + 1;
                to_visit.emplace(i);
                added[i] = true;
            }
    }
}

int main() {
    int n, m, x, y;
    fin >> n >> m >> x >> y;
    while (m--) {
        int a, b;
        fin >> a >> b;
        edges[a].emplace_back(b);
        edges[b].emplace_back(a);
    }

    calc_dist(x, dist_x);
    calc_dist(y, dist_y);

    int opt_len = dist_x[y]; //optimal length
    bitset<N> valid;
    for (int i = 1; i <= n; ++i)
        if (dist_x[i] + dist_y[i] == opt_len) {
            ++cnt[dist_x[i]];
            valid[i] = true;
        }

    queue<int> ans;
    for (int i = 1; i <= n; ++i)
        if (valid[i] && cnt[dist_x[i]] == 1)
            ans.emplace(i);

    fout << ans.size() << "\n";
    while (!ans.empty()) {
        fout << ans.front() << " ";
        ans.pop();
    }

    return 0;
}
