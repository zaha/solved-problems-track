#include <fstream>
using namespace std;

int main() {
    ifstream fin("filme.in");
    ofstream fout("filme.out");

    int n, m, times[10001], temp1, temp2, pos_min, contor = 0;
    fin >> n >> m;
    for (int i = 1; i <= n; ++i) {
        fin >> temp1 >> temp2;
        times[i] = temp1 + temp2;
    }

    while (true) {
        int min = 100000001;
        for (int i = 1; i <= n; ++i) {
            if (times[i] < min) {
                min = times[i];
                pos_min = i;
            }
        }
        times[pos_min] = 100000001;

        m -= min;
        if (m >= 0) {
            ++contor;
        } else {
            break;
        }
    }

    fout << contor;
    return 0;
}