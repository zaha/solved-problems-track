#include <fstream>
#include <vector>
#include <queue>
using namespace std;

vector <int> muchii[100005];
queue <int> nevizitate;
int distanta[100005];

void BFS() {
    int nod;

    while (!nevizitate.empty()) {
        nod = nevizitate.front();
        nevizitate.pop();

        for (unsigned int i = 0; i < muchii[nod].size(); ++i) {
            int vecin = muchii[nod][i];

            if (distanta[vecin] == -1) {
                nevizitate.push(vecin);
                distanta[vecin] = distanta[nod] + 1;
            }
        }
    }
}

int main() {
    ifstream fin("bfs.in");
    ofstream fout("bfs.out");
    
    int n, m, s;
    fin >> n >> m >> s;

    for (int i = 1; i <= m; ++i) {
        int x, y;
        fin >> x >> y;
        muchii[x].push_back(y);
    }

    for (int i = 1; i <= n; ++i)
        distanta[i] = -1;

    distanta[s] = 0;
    nevizitate.push(s);

    BFS();

    for (int i = 1; i <= n; ++i)
        fout << distanta[i] << " ";
}
