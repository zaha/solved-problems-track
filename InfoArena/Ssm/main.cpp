#include <fstream>
using namespace std;

ifstream fin("ssm.in");
ofstream fout("ssm.out");

int main() {
    int n;
    fin >> n;

    int sum, start = 1, stop = 1;
    int tempSum, tempStart = 1;
    fin >> sum;
    tempSum = sum;

    for (int i = 2; i <= n; ++i) {
        int temp;
        fin >> temp;

        tempSum += temp;
        if (tempSum < 0) {
            if (sum < 0 && tempSum > sum) {
                sum = tempSum;
                start = stop = i;
            }

            tempSum = 0;
            tempStart = i+1;
        } else {
            if (tempSum > sum) {
                sum = tempSum;
                start = tempStart;
                stop = i;
            } else if (tempSum == sum) {
                if (start - stop > i - tempStart) { // cauta cea mai scurta subsecventa
                    sum = tempSum;
                    start = tempStart;
                    stop = i;
                }
            }
        }
    }

    fout << sum << " " << start << " " << stop;
}