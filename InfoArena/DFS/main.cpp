#include <fstream>
#include <vector>
using namespace std;

vector <int> muchii[100005];
bool vizitat[100005];

void dfs(int nod) {
    vizitat[nod] = true;

    for (unsigned int i = 0; i < muchii[nod].size(); ++i) {
        int vecin = muchii[nod][i];
        if (!vizitat[vecin]) 
            dfs(vecin);
    }
}

int main() {
    ifstream fin("dfs.in");
    ofstream fout("dfs.out");
    
    int n, m, insule = 0;
    fin >> n >> m;

    for (int i = 1; i <= m; ++i) {
        int x, y;
        fin >> x >> y;
        muchii[x].push_back(y);
        muchii[y].push_back(x);
    }

    for (int i = 1; i <= n; ++i)
        if (!vizitat[i]) {
            ++insule;
            dfs(i);
        }

    fout << insule;
    return 0;
}
