#include <fstream>
#include <iostream>
#include <queue>
using namespace std;

int main() {
    ifstream fin("maxd.in");
    ofstream fout("maxd.out");
    int a, b;
    fin >> a >> b;
    queue <int> numbers;
    int max_divs = 0;
    
    for (int i = a; i <= b; ++i) {
        int contor = 2; //1 and number
        if (i == 1) {
            contor = 1;
        }
        for (int j = 2; j < 10; j += 2) {
            int k = j;
            while (i % k == 0) {
                if (i / k == k || i / k == k / 2) {
                    ++contor;
                    break;
                } else if (i / k < k) {
                    break;
                }
                contor += 2;
                k *= 2;
            }
            if (j == 2) {
                --j;
            }
        }
        if (contor > max_divs) {
            while (!numbers.empty()) {
                numbers.pop();
            }
            numbers.push(i);
            max_divs = contor;
        } else if (max_divs == contor) {
            numbers.push(i);
        }
    }

    fout << numbers.front() << " " << max_divs << " " << numbers.size();
    return 0;
}