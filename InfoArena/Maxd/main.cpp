#include <fstream>
#include <queue>
using namespace std;

int main() {
    ifstream fin("maxd.in");
    ofstream fout("maxd.out");
    int a, b;
    fin >> a >> b;
    queue <int> numbers;
    int max_divs = 0;
    
    for (int i = a; i <= b; ++i) {
        int contor = 2; //1 and number
        if (i == 1) {
            contor = 1;
        }
        for (int j = 2; j < i; ++j) {
            if (i % j == 0) {
                ++contor;
            }
        }
        if (contor > max_divs) {
            while (!numbers.empty()) {
                numbers.pop();
            }
            numbers.push(i);
            max_divs = contor;
        } else if (max_divs == contor) {
            numbers.push(i);
        }
    }

    fout << numbers.front() << " " << max_divs << " " << numbers.size();
    return 0;
}