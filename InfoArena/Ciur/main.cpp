#include <fstream>
#include <bitset>
using namespace std;

ifstream fin("ciur.in");
ofstream fout("ciur.out");

int n;
bitset<2000005> notPrime;

int main() {
    fin >> n;

    int primes = 1;
    for (int i = 3; i <= n; i += 2)
        if (!notPrime[i]) {
            ++primes;
            for (int j = i; j <= n; j += i)
                notPrime[j] = true;
        }

    fout << primes;
    return 0;
}