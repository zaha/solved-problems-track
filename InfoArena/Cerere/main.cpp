#include <bits/stdc++.h>
using namespace std;

int stramosi[100005];
vector <int> arbore[100005];
bitset <100005> areTata;
int parcurgeri[100005];

deque <int> trecut;
void calcParc(int maimuta) {
    for (unsigned int i = 0; i < arbore[maimuta].size(); ++i) {
        int nepot = arbore[maimuta][i];
        
        if (stramosi[nepot] > 0)
            trecut.push_back((trecut[trecut.size() - stramosi[nepot]]) + 1);
        else
            trecut.push_back(0);

        parcurgeri[nepot] = trecut.back();
        calcParc(nepot);
        trecut.pop_back();
    }
}

int main() {
    ifstream fin("cerere.in");
    ofstream fout("cerere.out");

    int n;
    fin >> n;

    queue <int> posibileRadacini;
    for (int i = 1; i <= n; ++i) {
        fin >> stramosi[i];

        if (stramosi[i] == 0)
            posibileRadacini.push(i);
    }

    for (int i = 1; i < n; ++i) {
        int x, y;
        fin >> x >> y;
        arbore[x].push_back(y);
        areTata[y] = true;
    }

    //Cauta radacina
    int radacina;
    while (!posibileRadacini.empty()) {
        if (!areTata[posibileRadacini.front()]) {
            radacina = posibileRadacini.front();
            break;
        }
        posibileRadacini.pop();
    }

    trecut.push_back(0);
    calcParc(radacina);

    for (int i = 1; i <= n; ++i)
        fout << parcurgeri[i] << " ";

    return 0;
}
