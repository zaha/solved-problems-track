#include <fstream>
#include <vector>
#define ll long long
using namespace std;

ifstream fin("secv2.in");
ofstream fout("secv2.out");

ll n, k;
vector<ll> sums;

int main() {
	fin >> n >> k;

	ll temp;
	fin >> temp;
	sums.emplace_back(temp);

	for (ll i = 1; i < n; ++i) {
		fin >> temp;
		sums.emplace_back(sums[i - 1] + temp);
	}

	int start = 0, stop = 0, sum = -1300000000, lowest = 0;
	for (int i = k - 1; i < n; ++i) {
		if (i >= k && sums[i - k] < sums[lowest])
			lowest = i - k;

		if (sums[lowest] < 0 &&  i >= k) {
			if (sums[i] - sums[lowest] > sum) {
				sum = sums[i] - sums[lowest];
				start = lowest + 2;
				stop = i + 1;
			}
		}
		else {
			if (sums[i] > sum) {
				sum = sums[i];
				start = 1;
				stop = i + 1;
			}
		}
	}

	fout << start << " " << stop << " " << sum;
	return 0;
}