#include <fstream>
#include <iostream>
using namespace std;

int main() {
    ifstream fin("difprim.in");
    ofstream fout("difprim.out");
    int a, b, firstNum;
    fin >> a >> b;

    for (int i = a; i <= b; ++i) {
        if (i % 2 != 0 && i % 3 != 0 && i % 5 != 0 && i % 7 != 0) {
            fout << i << " ";
            break;
        }
    }

    fout.close();
    fout.open("difprim.out");
    fout << -1;
    return 0;
}