#include <bits/stdc++.h>
#define ll long long
using namespace std;

ifstream fin("submatrix.in");
ofstream fout("submatrix.out");

const int N = 307;
int n, m, k, ans;
int mx[N][N];

void build_mx() {
    map<int, int> id;
    int cnt = 1;

    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= m; ++j)
            if (!id[mx[i][j]]) {
                id[mx[i][j]] = cnt;
                ++cnt;
            }

    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= m; ++j)
            mx[i][j] = id[mx[i][j]];
}

void get_ans(int start_i, int start_j) {
    int len_i = start_i, len_j = start_j;
    int cnt = 0, frec[N * N];
    bitset<N * N> seen;

    for (int i = start_i; i <= n; ++i) {
        int j = i - (start_i - start_j);
        if (j > m)
            break;

        for (int lin = len_i; lin <= i; ++lin) {
            if (!seen[mx[lin][j]]) {
                frec[mx[lin][j]] = 0;
                seen[mx[lin][j]] = true;
                ++cnt;
            }
            ++frec[mx[lin][j]];
        }
        for (int col = len_j; col < j; ++col) {
            if (!seen[mx[i][col]]) {
                frec[mx[i][col]] = 0;
                seen[mx[i][col]] = true;
                ++cnt;
            }
            ++frec[mx[i][col]];
        }

        while (cnt > k) {
            for (int lin = len_i; lin <= i; ++lin) {
                --frec[mx[lin][len_j]];
                if (!frec[mx[lin][len_j]]) {
                    --cnt;
                    seen[mx[lin][len_j]] = false;
                }
            }
            for (int col = len_j + 1; col <= j; ++col) {
                --frec[mx[len_i][col]];
                if (!frec[mx[len_i][col]]) {
                    --cnt;
                    seen[mx[len_i][col]] = false;
                }
            }

            ++len_i;
            ++len_j;
        }
        ans = max(ans, i - len_i + 1);
    }
}

int main() {
    fin >> n >> m >> k;
    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= m; ++j)
            fin >> mx[i][j];
    build_mx();

    for (int i = 1; i <= n; ++i)
        get_ans(i, 1);
    for (int j = 2; j <= m; ++j)
        get_ans(1, j);
    fout << ans;
}
