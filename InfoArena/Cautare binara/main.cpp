#include <fstream>
#include <vector>
#include <algorithm>
using namespace std;

ifstream fin("cautbin.in");
ofstream fout("cautbin.out");

vector<int> nums;

int main() {
    int n;
    fin >> n;
    while (n--) {
        int temp;
        fin >> temp;
        nums.emplace_back(temp);
    }

    int m;
    fin >> m;
    while (m--) {
        int type, num;
        fin >> type >> num;

        if (!type) {
            vector<int>::iterator it = upper_bound(nums.begin(), nums.end(), num);
            if (nums[distance(nums.begin(), it) - 1] == num)
                fout << distance(nums.begin(), it) << "\n";
            else
                fout << "-1\n";
        } else if (type == 1) {
            vector<int>::iterator it = upper_bound(nums.begin(), nums.end(), num);
            fout << distance(nums.begin(), it) << "\n";
        } else {
            vector<int>::iterator it = lower_bound(nums.begin(), nums.end(), num);
            fout << distance(nums.begin(), it) + 1 << "\n";
        }
    }
}