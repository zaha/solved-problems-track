#include <fstream>
#include <bitset>
using namespace std;

ifstream fin("submultimi.in");
ofstream fout("submultimi.out");

bitset<20> submultime;
int n;

void output() {
    bool found = false;
    for (int i = 1; i <= n; ++i)
        if (submultime[i]) {
            fout << i << " ";
            found = true;
        }

    if (found)
        fout << "\n";
}

void genSubmultimi(int nr) {
    if (nr > n)
        return;

    submultime[nr] = true;
    output();
    genSubmultimi(nr + 1);

    submultime[nr] = false;
    genSubmultimi(nr + 1);
}

int main() {
    fin >> n;
    genSubmultimi(1);
    return 0;
}