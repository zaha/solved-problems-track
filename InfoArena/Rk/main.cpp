#include <fstream>
using namespace std;

int main() {
    ifstream fin("rk.in");
    ofstream fout("rk.out");

    int n, q, k, r;
    int v[200003];
    fin >> n;
    for (int i = 1; i <= n; ++i) {
        fin >> v[i];
    }
    fin >> q;
    for (int j = 1; j <= q; ++j) {
        fin >> r >> k;
        int contor = 0;
        int putere = 2;
        if (k == 0) {
            putere = 1;
        }
        for (int i = 2; i <= k; ++i) {
            putere *= 2;
        }
        for (int i = 1; i <= n; ++i) {
            if (v[i] % putere == r) {
                ++contor;
            }
        }
        fout << contor << "\n";
    }
    return 0;
}