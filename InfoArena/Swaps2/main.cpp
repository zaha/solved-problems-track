#include <fstream>
#include <queue>
using namespace std;

int main() {
    ifstream fin("swaps2.in");
    ofstream fout("swaps2.out");

    int n;
    fin >> n;
    char s[n];
    for (int i = 0; i < n; ++i) {
        fin >> s[i];
    }

    int contor = 0;
    queue <int> pozitii;
    bool finish = false;
    for (int i = n; i >= 0; --i) {
        if (finish) {
            break;
        }
        if (s[i] == '0') {
            for (int j = 0; j <= i; ++j) {
                if (j == i) {
                    finish = true;
                    break;
                }
                if (s[j] == '1') {
                    pozitii.push(j+1);
                    pozitii.push(i+1);
                    ++contor;
                    s[j] = '0';
                    break;
                }
                
            }
        }
    }

    fout << contor << "\n";
    while (!pozitii.empty()) {
        fout << pozitii.front() << " ";
        pozitii.pop();
        fout << pozitii.front() << "\n";
        pozitii.pop();
    }
}