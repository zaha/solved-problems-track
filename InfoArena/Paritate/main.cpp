#include <fstream>
#include <string>
#include <queue>
#include <bitset>
using namespace std;

ifstream fin("paritate.in");
ofstream fout("paritate.out");

string text, transformat;
bitset <10> caracter;
queue <int> incorecte;
int curNum = 0;

void calcChar() {
    int sum = 0, curVal = 1;
    for (int i = 8; i > 1; --i) {
        int temp;
        if (caracter[i])
            temp = 1;
        else
            temp = 0;

        sum += temp * curVal;
        curVal *= 2;
    }

    transformat.push_back(sum);
}

void check() {
    int pozitive = 0;
    for (int i = 2; i <= 8; ++i)
        if (caracter[i])
            ++pozitive;
    
    if ((caracter[1] && !(pozitive & 1)) || (!caracter[1] && (pozitive & 1)))
        incorecte.emplace(curNum);
    else
        calcChar();
}

int main() {
    fin >> text;

    int curBit = 0;
    while (curBit < text.size() - 2) {
        for (int i = 1; i <= 8; ++i) {
            if (text[curBit] == '1')
                caracter[i] = true;
            else
                caracter[i] = false;

            ++curBit;
        }

        check();
        ++curNum;
    }
    
    if (incorecte.empty()) {
        fout << "DA\n";
        fout << transformat;
    } else {
        fout << "NU\n";
        while (!incorecte.empty()) {
            fout << incorecte.front() << " ";
            incorecte.pop();
        }
    }

    return 0;
}