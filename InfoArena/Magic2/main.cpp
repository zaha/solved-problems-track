#include <fstream>
#include <iostream>
using namespace std;

int main() {
    ifstream fin("magic2.in");
    ofstream fout("magic2.out");

    int n, matrice[101][101], sume_lin[101], sume_col[101], corect, sume[101], sum1 = -1, sum2 = -1, sum3 = -1, sum, err1, err2;

    fin >> n;
    for (int i = 1; i <= n; ++i) {
        sum = 0;
        for (int j = 1; j <= n; ++j) {
            fin >> matrice [i][j];
            sum += matrice[i][j];
            if (i > 1)
                sume_col[j] += matrice[i][j];
            else
                sume_col[j] = matrice[i][j];
        }
        if (!(sum1 >= 0)) {
            sum1 = sum;
        } else if (!(sum2 >= 0) && sum != sum1) {
            sum2 = sum;
        } else if (!(sum3 >= 0)) {
            sum3 = sum;
        }
        cout << sum1 << " " << sum2 << " " << sum3 << "\n";
 
        sume_lin[i] = sum;
        if (sume[sum] > 0) {
            ++sume[sum];
        } else {
            sume[sum] = 1;
        }
    }
    if (sum2 == -1) {
        fout << "magic";
        return 0;
    } else {
        fout << "nu este magic\n";
    }
    if (sume[sum1] > 2) {
        corect = sum1;
        err1 = sum2;
        err2 = sum3;
    } else if (sume[sum2] > 2) {
        corect = sum2;
        err1 = sum1;
        err2 = sum3;
    } else {
        corect = sum3;
        err1 = sum1;
        err2 = sum2;
    }

    cout << err1 << " " << err2 << "\n";
    int x1, y1, x2, y2;
    for (int i = 1; i <= n; ++i) {
        if (err1 == sume_lin[i]) {
            x1 = i;
            fout << i << " ";
            break;
        }
    }
    for (int i = 1; i <= n; ++i) {
        if (err1 == sume_col[i]) {
            y1 = i;
            fout << i << " ";
            break;
        }
    }
    for (int i = 1; i <= n; ++i) {
        if (err2 == sume_lin[i] && i != x1) {
            x2 = i;
            fout << i << " ";
            break;
        }
    }
    for (int i = 1; i <= n; ++i) {
        if (err2 == sume_col[i] && i != y1) {
            y2 = i;
            fout << i << "\n";
            break;
        }
    }
    cout << x1 << " " << y1 << " " << x2 << " " << y2;

    

    fout << matrice[x1][y1] + (corect - err1) << " " << matrice[x2][y2] + (corect - err2);
}