#include <bits/stdc++.h>
#define ll long long
using namespace std;

ifstream fin("zlego.in");
ofstream fout("zlego.out");

const int N = 25e4 + 7;
int n, pref[N], pos[N];
ll val[N];

void make_pref() {
    int k = 0;
    for (int i = 2; i <= n; ++i) {
        while (k && pos[k + 1] != pos[i])
            k = pref[k];
        if (pos[k + 1] == pos[i])
            ++k;
        pref[i] = k;
    }
}

void solve() {
    fin >> n;
    for (int i = 1; i <= n; ++i)
        fin >> pos[i];
    for (int i = 1; i <= n; ++i)
        fin >> val[i];

    make_pref();
    for (int i = n; i; --i)
        val[pref[i]] += val[i];
    for (int i = 1; i <= n; ++i)
        fout << val[i] << "\n";
}

int main() {
    int t;
    fin >> t;
    while (t--)
        solve();
}
