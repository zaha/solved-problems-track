#include <fstream>
using namespace std;

ifstream fin("cartele.in");
ofstream fout("cartele.out");

int sablon[51][51], cartela[51][51], temp[51][51];
int n, C;

void rotate() {
    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= n; ++j)
            temp[i][j] = cartela [n-j+1][i];
    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= n; ++j)
            cartela[i][j] = temp[i][j];

    // salveaza spatele cartelei in temp
    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= n; ++j)
            temp[i][j] = cartela[i][n-j+1];
}

bool check(bool tmp) {
    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= n; ++j) {
            if (!tmp) {
                if (cartela[i][j] != sablon[i][j])
                    return false;
            } else {
                if (temp[i][j] != sablon[i][j])
                    return false;
            }
        }

    return true;
}

int main() {
    fin >> n >> C;
    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= n; ++j)
            fin >> sablon[i][j];

    while (C--) {
        for (int i = 1; i <= n; ++i)
            for (int j = 1; j <= n; ++j)
                fin >> cartela[i][j];
        
        bool valid = false;
        for (int l = 1; l <= 4; ++l) {
            rotate();
            
            if (check(false) || check(true)) {
                valid = true;
                break;
            }
        }

        if (valid)
            fout << "1\n";
        else
            fout << "0\n";
    }

    return 0;
}