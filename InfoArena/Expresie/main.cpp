#include <fstream>
using namespace std;

ifstream fin("expresie.in");
ofstream fout("expresie.out");

int nr[1005], produse[1005];
int maxProd1, maxProd2;

int main() {
    int n;
    fin >> n;

    for (int i = 1; i <= n; ++i) {
        int temp;
        fin >> temp;

        nr[i] = temp;
        produse[i] = nr[i] * nr[i-1];

        if (i == 1)
            continue;
        if (produse[i] >= produse[maxProd1] && produse[i] >= (nr[i] + nr[i-1])) {
            maxProd2 = maxProd1;
            maxProd1 = i;
        } else if (produse[i] > produse[maxProd2] && produse[i] >= (nr[i] + nr[i-1])) {
            maxProd2 = i;
        }
    }

    if (maxProd1 == 0)
        maxProd1 = 1;
    if (maxProd2 == 0)
        maxProd2 = 2;

    long long total = 0;
    for (int i = 1; i <= n; ++i) {
        if (i == maxProd1 || i == maxProd2)
            continue;

        if ((i + 1) == maxProd1 || (i + 1) == maxProd2)
            total += nr[i] * nr[i+1];
        else
            total += nr[i];
    }
    
    fout << total;
    return 0;
}