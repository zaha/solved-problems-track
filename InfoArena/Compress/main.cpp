#include <fstream>
using namespace std;

int main() {
    ifstream fin("compress.in");
    ofstream fout("compress.out");
    char litera, litera_curenta = '9';
    int contor = 0;

    while (fin >> litera) {
        if (litera != litera_curenta) {
            if (contor != 0) {
                fout << litera_curenta << contor;
            }
            litera_curenta = litera;
            contor = 1;
        } else {
            ++contor;
        }
    }
    fout << litera_curenta << contor;
    return 0;
}