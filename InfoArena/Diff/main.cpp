#include <bits/stdc++.h>
#define ll long long
using namespace std;

ifstream fin("diff.in");
ofstream fout("diff.out");

const int N = 1e5 + 7;
pair<int, int> neg[N], pos[N];

void replace(int k, int l, int r) {
    if (k < 0) {
        k = abs(k);
        neg[k] = make_pair(l, r);
    } else
        pos[k] = make_pair(l, r);
}

int main() {
    int n, m;
    fin >> n >> m;

    int cur = 0, mx = 0, mn = 0, pos_mx = 0, pos_mn = 0;
    for (int i = 1; i <= n; ++i) {
        int t;
        fin >> t;

        if (t)
            ++cur;
        else
            --cur;

        replace(cur - mn, pos_mn + 1, i);
        replace(cur - mx, pos_mx + 1, i);

        if (cur < mn) {
            mn = cur;
            pos_mn = i;
        }
        if (cur > mx) {
            mx = cur;
            pos_mx = i;
        }
    }

    while (m--) {
        int k;
        fin >> k;

        if (k < 0) {
            k = abs(k);
            if (neg[k].first)
                fout << neg[k].first << " " << neg[k].second << "\n";
            else
                fout << "-1\n";
        } else {
            if (pos[k].first)
                fout << pos[k].first << " " << pos[k].second << "\n";
            else
                fout << "-1\n";
        }
    }
}
