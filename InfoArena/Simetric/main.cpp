#include <fstream>
using namespace std;

int main() {
    ifstream fin("simetric.in");
    ofstream fout("simetric.out");

    int n, m, l;
    fin >> n >> m;
    if (n > m)
        l = n;
    else
        l = m;
    
    int total = 0;
    int matrice[401][401];
    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= m; ++j) {
            fin >> matrice[i][j];
            ++total;
        }
    fout << total << "\n";
    total = 0;
    bool notSim = false;
    
    for (int r = 2; r <= l; ++r) {
        for (int i = 1; i <= n - r + 1; ++i) {
            for (int j = 1; j <= m - r + 2; ++j) {
                for (int a = i; a <= i + r - 1; ++a) {
                    for (int b = j; b <= j + r - 1; ++b) {
                        if (matrice[a][b] != matrice[b][a]) {
                            notSim = true;
                            break;
                        }
                    }
                    if (notSim) {
                        break;
                    }
                }
                if (notSim) {
                    notSim = false;
                    continue;
                }
                ++total;
            }
        }
        if (total != 0) fout << total / 2 << "\n";
        total = 0;
    }

    return 0;
}