#include <bits/stdc++.h>
using namespace std;

ifstream fin("search.in");
ofstream fout("search.out");

const int N = 107, M = 5007;
int n, m, len[N];
int next_pos[N][M][26], last_pos[26];
int missing[N];
stack<int> pos[N];

int main() {
    fin >> n >> m;
    for (int i = 1; i <= n; ++i) {
        string file;
        fin >> file;
        len[i] = file.size();

        for (int c = 0; c < 26; ++c)
            last_pos[c] = len[i];

        for (int j = len[i] - 1; j >= 0; --j) {
            for (int c = 0; c < 26; ++c)
                next_pos[i][j + 1][c] = last_pos[c];
            last_pos[file[j] - 'a'] = j;
        }

        for (int c = 0; c < 26; ++c)
            next_pos[i][0][c] = min(last_pos[c], next_pos[i][1][c]);
    }

    for (int i = 1; i <= n; ++i)
        pos[i].emplace(-1);

    int ans = n;
    while (m--) {
        char c;
        fin >> c;

        for (int i = 1; i <= n; ++i) {
            if (c != '-') {
                if (missing[i])
                    ++missing[i];
                else {
                    int np = next_pos[i][pos[i].top() + 1][c - 'a'];
                    if (np == len[i]) {
                        --ans;
                        ++missing[i];
                    } else
                        pos[i].emplace(np);
                }
            } else {
                if (missing[i]) {
                    --missing[i];
                    if (!missing[i])
                        ++ans;
                } else
                    pos[i].pop();
            }
        }
        fout << ans << "\n";
    }
}
