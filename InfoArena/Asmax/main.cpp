#include <bits/stdc++.h>
using namespace std;

int valori[16005];
vector <int> muchii[16005];
bitset <16005> vizitat;
int maxim = -1005;

int find(int nod) {
    int suma = valori[nod];
    for (unsigned int i = 0; i < muchii[nod].size(); ++i) {
        int vecin = muchii[nod][i];
        if (vizitat[vecin])
            continue;
        vizitat[vecin] = true;

        int temp = find(vecin);
        if (temp > 0)
            suma += temp;
    }

    if (suma > maxim)
        maxim = suma;
    return suma;
}

int main() {
    ifstream fin("asmax.in");
    ofstream fout("asmax.out");

    int n;
    fin >> n;

    for (int i = 1; i <= n; ++i)
        fin >> valori[i];

    for (int i = 1; i < n; ++i) {
        int x, y;
        fin >> x >> y;
        
        muchii[x].emplace_back(y);
        muchii[y].emplace_back(x);
    }

    //Cauta o radacina valida (pozitiva sau nula)
    int radacina = 1;
    while (radacina < 0)
        ++radacina;
    
    vizitat[radacina] = true;
    find(radacina);
    fout << maxim;
    return 0;
}
