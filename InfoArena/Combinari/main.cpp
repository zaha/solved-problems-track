#include <fstream>
using namespace std;

ifstream fin("combinari.in");
ofstream fout("combinari.out");

int n, k;
int perm[20];

void genPerm(int level, int start) {
    for (int i = start; i <= n - k + level; ++i) {
        perm[level] = i;

        if (level == k) {
            for (int j = 1; j <= k; ++j)
                fout << perm[j] << " ";
            fout << "\n";
        } else
            genPerm(level + 1, i + 1);
    }
}

int main() {
    fin >> n >> k;
    genPerm(1, 1);
    return 0;
}