#include <fstream>
#include <vector>
#include <bitset>
using namespace std;

ifstream fin("panouri.in");
ofstream fout("panouri.out");

int n, t, k, minLen;
vector<int> nums;
bitset<20005> wanted;
int presence[200005];

int main() {
    fin >> n >> t;
    for (int i = 1; i <= n; ++i) {
        int temp;
        fin >> temp;
        nums.emplace_back(temp);
    }
    for (int i = 1; i <= t; ++i) {
        int temp;
        fin >> temp;
        wanted[temp] = true;
    }

    int front = 0, back = -1;
    for (unsigned i = 0; i < nums.size(); ++i) {
        ++back;
        if (k < t && wanted[nums[back]] && !presence[nums[back]])
            ++k;
        ++presence[nums[back]];

        if (k == t) {
            while (!wanted[nums[front]] || presence[nums[front]] > 1) {
                --presence[nums[front]];
                ++front;
            }
            if (back - front < minLen || !minLen)
                minLen = back - front;
        }
    }

    fout << minLen;
    return 0;
}
