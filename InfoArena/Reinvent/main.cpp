#include <bits/stdc++.h>
using namespace std;

ifstream fin("reinvent.in");
ofstream fout("reinvent.out");

const int N = 1e5 + 7;
vector<int> streets[N];
int root[N], dist[N];

int main() {
    int n, m, x;
    fin >> n >> m >> x;

    while (m--) {
        int a, b;
        fin >> a >> b;
        streets[a].emplace_back(b);
        streets[b].emplace_back(a);
    }

    queue<int> to_visit;
    while (x--) {
        int node;
        fin >> node;
        to_visit.emplace(node);
        root[node] = node;
    }

    while (!to_visit.empty()) {
        int node = to_visit.front();
        to_visit.pop();

        for (int i : streets[node])
            if (!root[i]) {
                root[i] = root[node];
                to_visit.emplace(i);
                dist[i] = dist[node] + 1;
            } else if (root[node] != root[i]) {
                fout << dist[node] + dist[i] + 1;
                return 0;
            }
    }

    fout << -1; //just in case
    return 0;
}
