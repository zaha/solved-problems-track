#include <bits/stdc++.h>
using namespace std;

ifstream fin("grarb.in");
ofstream fout("grarb.out");

const int N = 1e5 + 7;
vector<int> graph[N];
bitset<N> visited, eliminated[N];

void dfs(int node) {
    visited[node] = true;
    for (int i : graph[node])
        if (!visited[i])
            dfs(i);
}

int main() {
    int n, m;
    fin >> n >> m;
    for (int i = 1; i <= m; ++i) {
        int x, y;
        fin >> x >> y;
        graph[x].emplace_back(y);
        graph[y].emplace_back(x);
    }

    int to_add = -1;
    for (int i = 1; i <= n; ++i)
        if (!visited[i]) {
            ++to_add;
            dfs(i);
        }

    fout << (m + to_add) - (n - 1) << "\n" << to_add;
    return 0;
}
