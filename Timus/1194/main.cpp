#include <iostream>
using namespace std;

int groups[200005];

int main() {
    int n, k, handshakes = 0, single = 0, maried = 0;
    cin >> n >> k;

    while (single != n - k * 2 || maried != k) {
        int group, newGroups;
        cin >> group >> newGroups;

        if (groups[group] == 2)
            --maried;

        int hobbits = 0;
        while (newGroups--) {
            int g, s; // group, size
            cin >> g >> s;
            groups[g] = s;

            if (s == 1)
                ++single;
            if (s == 2)
                ++maried;

            handshakes += s * hobbits;
            hobbits += s;
        }
    }

    cout << handshakes;
    return 0;
}