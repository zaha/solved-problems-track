#include <iostream>
#include <queue>
using namespace std;

int main() {
    int n;
    cin >> n;

    if (n <= 2) {
        cout << 2;
        return 0;
    }

    queue <long long int> fibonacci;
    fibonacci.push(2);
    fibonacci.push(2);

    for (int i = 3; i <= n; ++i) {
        long long int temp = fibonacci.front();
        fibonacci.pop();
        
        temp += fibonacci.front();
        fibonacci.push(temp);
    }
    
    cout << fibonacci.back();
    return 0;
}
