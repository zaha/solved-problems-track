#include <iostream>

using namespace std;

int main()
{
    int n;
    cin >> n;
    int emperor = 0, macaroni = 0, little = 0;
    for (int i = 1; i <= n; ++i) {
        char type[15], penguin[10];
        cin >> type >> penguin;
        if (type[0] == 'E')
            ++emperor;
        else if (type[0] == 'M')
            ++macaroni;
        else
            ++little;
    }

    if (emperor > macaroni && emperor > little)
        cout << "Emperor Penguin";
    else if (macaroni > emperor && macaroni > little)
        cout << "Macaroni Penguin";
    else
        cout << "Little Penguin";

    return 0;
}
