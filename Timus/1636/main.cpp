#include <iostream>
using namespace std;

int main() {
    int q, z;
    cin >> q >> z;

    int penalty = 0;
    for (int i = 1; i <= 10; ++i) {
        int temp;
        cin >> temp;
        penalty += temp;
    }
    penalty *= 20;

    if ((z - penalty) >= q)
        cout << "No chance.";
    else
        cout << "Dirty debug :(";

    return 0;
}