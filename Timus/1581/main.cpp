#include <iostream>
using namespace std;

int main() {
    int n;
    cin >> n;

    int counter = 1, curnum;
    cin >> curnum;
    
    for (int i = 2; i <= n; ++i) {
        int num;
        cin >> num;

        if (num == curnum)
            ++counter;

        else {
            cout << counter << ' ' << curnum << ' ';
            counter = 1;
            curnum = num;
        }
    }
    cout << counter << ' ' << curnum;

    return 0;
}
