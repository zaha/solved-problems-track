#include <iostream>
#define ll long long
using namespace std;

int main() {
    int n, k;
    cin >> n >> k;

    ll total = k, zeroes = 1;
    for (int i = 2; i <= n; ++i) {
        ll temp = total;
        total *= k - 1;

        if (i != n)
            total += temp - zeroes;
        zeroes = temp - zeroes;
    }

    cout << total;
    return 0;
}