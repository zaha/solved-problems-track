#include <bits/stdc++.h>
#define ll long long
using namespace std;

int s, f;
ll prof[507];
vector<pair<int, ll>> pips[507];

void get_prof(int station) {
    if (station == f)
        return;
    prof[station] = -1;

    for (pair<int, ll> i : pips[station]) {
        if (!prof[i.first])
            get_prof(i.first);
        if (prof[i.first] > -1)
            prof[station] = max(prof[station], i.second + prof[i.first]);
    }
}

int main() {
    int n, m;
    cin >> n >> m;
    while (m--) {
        int a, b;
        ll c;
        cin >> a >> b >> c;
        pips[a].emplace_back(make_pair(b, c));
    }
    cin >> s >> f;

    get_prof(s);
    if (prof[s] == -1)
        cout << "No solution";
    else
        cout << prof[s];
    return 0;
}
