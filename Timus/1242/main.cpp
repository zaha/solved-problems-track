#include <bits/stdc++.h>
using namespace std;

const int N = 1e3 + 7;
vector<int> descendants[N], ancestors[N];
bitset<N> visited;

void dfs(int node, vector<int> *graph) {
    visited[node] = true;

    for (int i : graph[node])
        dfs(i, graph);
}

int main() {
    // If I don't read the input with getline, it will seg fault 75% of times
    int n;
    string temp;
    getline(cin, temp);
    n = (stoi(temp));

    while (true) {
        string row;
        getline(cin, row);
        if (row == "BLOOD") break;

        int x, y;
        stringstream ss(row);
        ss >> x >> y;
        ancestors[x].emplace_back(y);
        descendants[y].emplace_back(x);
    }

    int victim;
    while (cin >> victim) {
        dfs(victim, ancestors);
        dfs(victim, descendants);
    }

    bool found = false;
    for (int i = 1; i <= n; ++i)
        if (!visited[i]) {
            cout << i << ' ';
            found = true;
        }
    if (!found) cout << 0;
    return 0;
}
