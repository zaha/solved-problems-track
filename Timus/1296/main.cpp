#include <iostream>
using namespace std;

int main() {
    int n;
    cin >> n;

    int maxSum = 0, curSum = 0;
    for (int i = 1; i <= n; ++i) {
        int temp;
        cin >> temp;

        curSum += temp;
        if (curSum > maxSum)
            maxSum = curSum;
        else if (curSum <= 0)
            curSum = 0;
    }

    cout << maxSum;
    return 0;
}
