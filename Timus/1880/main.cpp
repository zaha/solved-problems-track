#include <iostream>
using namespace std;

int main() {
    int a, b, c, eigenvalues = 0;
    int arra[4005], arrb[4005], arrc[4005];
    
    cin >> a;
    for (int i = 1; i <= a; ++i)
        cin >> arra[i];

    cin >> b;
    for (int i = 1; i <= b; ++i)
        cin >> arrb[i];

    cin >> c;
    for (int i = 1; i <= c; ++i)
        cin >> arrc[i];

    int contorb = 1, contorc = 1;
    for (int i = 1; i <= a; ++i) {
        int target = arra[i];
        for (int j = contorb; j <= b && arrb[j] <= target; ++j) {
            ++contorb;
            if (arrb[j] == target)
                for (int k = contorc; k <= c && arrc[k] <= target; ++k) {
                    ++contorc;
                    if (arrc[k] == target)
                        ++eigenvalues;
                }
        }
    }

    cout << eigenvalues;
    return 0;
}
