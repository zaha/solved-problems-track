#include <iostream>
#include <vector>
#include <algorithm>
using namespace std;

int main() {
    int k, counter = 0;
    cin >> k;
    vector<int> groups;
    for (int i = 1; i <=k; ++i) {
        int temp;
        cin >> temp;
        groups.emplace_back(temp);
    }

    sort(groups.begin(), groups.end());
    for (int i = 0; i <= groups.size() / 2; ++i)
        counter += groups[i] / 2 + 1;

    //Multiset solution
    /*
    int pos = 1;
    auto it = groups.begin();
    do {
        int temp = *it;
        counter += temp / 2 + 1;

        ++pos;
        ++it;
    } while (pos <= (groups.size() / 2) + 1);
    */

    cout << counter;
    return 0;
}