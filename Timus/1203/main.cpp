#include <iostream>
#include <algorithm>
#include <vector>
using namespace std;

vector <int> times[30005];
int maxReports[30005];

int main() {
    int n;
    cin >> n;

    int minStart = 30100, maxEnd = -1;
    while (n--) {
        int start, end;
        cin >> start >> end;
        times[start].emplace_back(end);
        
        minStart = min(minStart, start);
        maxEnd = max(maxEnd, end);
    }

    for (int i = minStart; i <= maxEnd; ++i) {
        for (unsigned j = 0; j < times[i].size(); ++j)
            maxReports[times[i][j]] = max(maxReports[times[i][j]], maxReports[i-1] + 1);
        
        maxReports[i] = max(maxReports[i-1], maxReports[i]);
    }

    cout << maxReports[maxEnd];
    return 0;
}