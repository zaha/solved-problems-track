#include <iostream>
#include <set>
#include <string>

using namespace std;

int main()
{
    int n;
    cin >> n;
    multiset <string> shops;

    for (int i = 0; i <= n; ++i) {
        string temp;
        getline(cin, temp);
        shops.insert(temp);
    }

    int counter = -1;
    string temp;
    for (multiset <string> :: iterator i = shops.begin(); i != shops.end(); ++i) {
        if (*i == temp)
            ++counter;
        temp = *i;
    }

    cout << counter;
    return 0;
}
