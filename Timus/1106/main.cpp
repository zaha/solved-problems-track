#include <iostream>
#include <vector>
#include <queue>
#include <bitset>
using namespace std;

int n;
bool possible = true;
vector<int> friends[103];
queue<int> firstTeam;
bitset<103> inSecondTeam;
bitset<103> visited;

void calcTeams() {
    for (int i = 1; i <= n; ++i) {
        if (!friends[i].size()) {
            possible = false;
            return;
        }

        for (int j = 0; j < (int)friends[i].size(); ++j) {
            int neighbor = friends[i][j];
            if (visited[neighbor])
                continue;

            if (!inSecondTeam[i])
                inSecondTeam[neighbor] = true;
            else
                firstTeam.emplace(neighbor);
            visited[neighbor] = true;
        }
    }
}

int main() {
    cin >> n;

    for (int i = 1; i <= n; ++i) {
        int j;
        cin >> j;

        while (j) {
            friends[i].emplace_back(j);
            cin >> j;
        }
    }

    calcTeams();

    if (!possible)
        cout << 0;
    else {
        cout << firstTeam.size() << "\n";

        while (!firstTeam.empty()) {
            cout << firstTeam.front() << " ";
            firstTeam.pop();
        }
    }
}
