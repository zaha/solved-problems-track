#include <iostream>
using namespace std;

int calcMin(int a, int b, int c) {
    int minim;
    minim = a + b + c;
    minim = min(minim, a + b - c);
    minim = min(minim, a + b * c);
    minim = min(minim, a * b - c);
    minim = min(minim, a * b + c);
    minim = min(minim, a * b * c);
    minim = min(minim, a - b * c);
    minim = min(minim, a - b + c);
    minim = min(minim, a - b - c);

    return minim;
}

int main() {
    int a, b, c;
    cin >> a >> b >> c;

    int minim = calcMin(a, b, c);
    minim = min(minim, calcMin(a, c, b));
    minim = min(minim, calcMin(b, a, c));
    minim = min(minim, calcMin(b, c, a));
    minim = min(minim, calcMin(c, a, b));
    minim = min(minim, calcMin(c, b, a));

    cout << minim;
    return 0;
}
