#include <iostream>
#include <string>
using namespace std;

int charCost(char c) {
    if (c == 'a' || c == 'd' || c == 'g' || c == 'j' || c == 'm' || c == 'p' || c == 's' || c == 'v' || c == 'y' || c == '.' || c == ' ')
        return 1;

    else if (c == 'b' || c == 'e' || c == 'h' || c == 'k' || c == 'n' || c == 'q' || c == 't' || c == 'w' || c == 'z' || c == ',')
        return 2;

    else
        return 3;
}

int main() {
    string message;
    getline(cin, message);
    int cost = 0;
    
    for (unsigned int i = 0; i < message.size(); ++i)
        cost += charCost(message[i]);

    cout << cost;
    return 0;
}
