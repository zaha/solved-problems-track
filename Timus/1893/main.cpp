#include <iostream>
#include <string>
using namespace std;

int main() {
    string seat;
    cin >> seat;

    int row;
    if (seat[1] >= '0' && seat[1] <= '9') {
        row = (seat[0] - '0') * 10 + (seat[1] - '0');
        seat = seat[2];
    } else {
        row = seat[0] - '0';
        seat = seat[1];
    }

    if (row <= 2) {
        if (seat == "A" || seat == "D")
            cout << "window";
        else
            cout << "aisle";
    } else if (row <= 20) {
        if (seat == "A" || seat == "F")
            cout << "window";
        else
            cout << "aisle";
    } else {
        if (seat == "A" || seat == "K")
            cout << "window";
        else if (seat == "C" || seat == "D" || seat == "G" || seat == "H")
            cout << "aisle";
        else
            cout << "neither";
    }

    return 0;
}
