#include <iostream>
#include <vector>
using namespace std;

int n, m;
long long bytes[100003];
vector <int> network[100003];

int main() {
    cin >> n;
    for (int i = 1; i <= n; ++i)
        cin >> bytes[i];
    
    for (int i = 1; i < n; ++i) {
        int x, y;
        cin >> x >> y;
        network[x].emplace_back(y);
        network[y].emplace_back(x);
    }

    cin >> m;
    for (int i = 1; i <= m; ++i) {
        int type, v;
        cin >> type >> v;

        if (type == 2)
            cout << (long long)bytes[v] % 1000000007 << "\n";

        if (type == 1)
            for (unsigned int i = 0; i < network[v].size(); ++i)
                bytes[network[v][i]] += bytes[v];
    }

    return 0;
}