#include <iostream>
#include <vector>
using namespace std;

int main() {
    int n, m;
    cin >> n >> m;

    vector<int> nums;
    for (int i = 1; i <= n; ++i) {
        int temp;
        cin >> temp;
        nums.emplace_back(temp);
    }

    int pos = 0;
    while (m--) {
        ++pos;
        if (pos == n)
            pos = 0;
    }

    int counter = 0;
    while (counter < 10) {
        cout << nums[pos];
        ++pos;
        ++counter;

        if (pos == nums.size())
            pos = 0;
    }
}