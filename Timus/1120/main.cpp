#include <iostream>
#include <cmath>
#define ll long long
using namespace std;

ll gauss(ll x) {
    ll sum = (x + 1) * (x / 2);
    if (x & 1)
        sum += (x / 2) + 1;
    return sum;
}

int main() {
    ll n;
    cin >> n;

    for (ll p = sqrt(n) * 2; p >= 1; --p) {
        ll g = gauss(p - 1);
        if (n - g > 0 && !((n - g) % p)) {
            cout << (n - g) / p << " " << p;
            return 0;
        }
    }
}