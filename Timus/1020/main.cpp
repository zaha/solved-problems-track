#include <iostream>
#include <cmath>
#include <iomanip>
using namespace std;

int n;
double r;
double length = 0.0;

int main() {
    cin >> n >> r;

    if (n == 1) {
        cout << fixed << setprecision(2) << 2 * 3.1415 * r;
        return 0;
    }

    // reading positions
    double firstX, firstY, lastX, lastY;
    for (int i = 1; i <= n; ++i) {
        double x, y;
        cin >> x >> y;

        if (i == 1) {
            firstX = x;
            firstY = y;
            lastX = x;
            lastY = y;
            continue;
        }

        double side1, side2, hypotenuse;
        side1 = max(x, lastX) - min(x, lastX);
        side2 = max(y, lastY) - min(y, lastY);
        hypotenuse = sqrt((side1 * side1) + (side2 * side2));
        length += hypotenuse;
        lastX = x;
        lastY = y;

        // connect last position with first position
        if (i == n) {
            double side1, side2, hypotenuse;
            side1 = max(x, firstX) - min(x, firstX);
            side2 = max(y, firstY) - min(y, firstY);
            hypotenuse = sqrt((side1 * side1) + (side2 * side2));
            length += hypotenuse;
        }
    }

    cout << fixed << setprecision(2) << length + (2 * 3.1415 * r);
    return 0;
}