#include <iostream>
using namespace std;

int main()
{
    long long n, counter = 0;
    cin >> n;

    for (int i = 0; i <= n; ++i)
        for (int j = i; j <= n; ++j)
            counter += i+j;

    cout << counter;
    return 0;
}
