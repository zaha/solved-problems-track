#include <iostream>
#include <string>
using namespace std;

int main()
{
    string secret;
    cin >> secret;

    int numOf26, lastStep2;
    if (secret[0] - 'a' < 5) {
        lastStep2 = 26 + (secret[0] - 'a');
        numOf26 = 1;
    } else {
        lastStep2 = (secret[0] - 'a');
        numOf26 = 0;
    }
    cout << (char)('a' + lastStep2 - 5);

    for (int i = 1; i < secret.size(); ++i) {
        int num = secret[i] - 'a';
        if (secret[i] < secret[i-1])
            ++numOf26;

        int step2 = numOf26 * 26 + num;
        cout << (char)('a' + (step2 - lastStep2));
        lastStep2 = step2;
    }

    return 0;
}
