#include <iostream>
#include <queue>
#include <string>
#include <set>

using namespace std;

int main()
{
    int n;
    cin >> n;

    queue <string> hieroglyphs;
    for (int i = 1; i <= n; ++i) {
        string temp;
        cin >> temp;
        hieroglyphs.push(temp);
    }

    char key;
    cin >> key;

    set <string> matches;
    while (!hieroglyphs.empty()) {
        string temp = hieroglyphs.front();
        if (temp[0] == key)
            matches.insert(temp);
        hieroglyphs.pop();
    }

    for (set <string> :: iterator i = matches.begin(); i != matches.end(); ++i)
        cout << *i << "\n";

    return 0;
}
