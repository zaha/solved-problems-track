#include <iostream>
using namespace std;

int main() {
    int a, b;
    cin >> a >> b;

    int c = (b - a + 1) / 2;

    if (a & 1 && b & 1)
        ++c;

    cout << c;
    return 0;
}
