#include <iostream>
using namespace std;

int main()
{
    int t;
    cin >> t;

    while (t--) {
        int n, k;
        cin >> n >> k;

        long long fights = 0, fighters = 0;
        for (int i = 1; i <= k; ++i) {
            int team = (n+k-i)/k;

            fights += team * fighters;
            fighters += team;
        }

        cout << fights << "\n";
    }

    return 0;
}
