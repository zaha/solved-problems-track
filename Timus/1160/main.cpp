#include <bits/stdc++.h>
using namespace std;

vector<pair<int, int>> cables[1007];

int gen_plan(queue<pair<int, int>> &plan) {
    priority_queue<tuple<int, int, int>, vector<tuple<int, int, int>>,
                   greater<tuple<int, int, int>>> next_mov;
    bitset<1007> visited;
    int max_len = 0;

    next_mov.emplace(make_tuple(0, 0, 1)); //generate an initial queue

    while (true) {
        while (!next_mov.empty() && visited[get<2>(next_mov.top())])
            next_mov.pop();
        if (next_mov.empty())
            break;

        tuple<int, int, int> cable = next_mov.top();
        next_mov.pop();
        plan.emplace(make_pair(get<1>(cable), get<2>(cable)));
        visited[get<2>(cable)] = true;
        max_len = max(max_len, get<0>(cable));

        for (pair<int, int> i : cables[get<2>(cable)])
            next_mov.emplace(make_tuple(i.first, get<2>(cable), i.second));
    }

    plan.pop(); //remove first cable used to generate the initial queue
    return max_len;
}

int main() {
    int n, m;
    cin >> n >> m;
    while (m--) {
        int a, b, c;
        cin >> a >> b >> c;
        cables[a].emplace_back(make_pair(c, b));
        cables[b].emplace_back(make_pair(c, a));
    }

    queue<pair<int, int>> ans;
    cout << gen_plan(ans) << "\n" << ans.size() << "\n";
    while (!ans.empty()) {
        pair<int, int> i = ans.front();
        ans.pop();
        cout << i.first << " " << i.second << "\n";
    }

    return 0;
}
