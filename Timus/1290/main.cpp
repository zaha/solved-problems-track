#include <iostream>
#include <set>
using namespace std;

int main() {
    int n;
    cin >> n;

    multiset<int> nums;
    for (int i = 1; i <= n; ++i) {
        int temp;
        cin >> temp;
        nums.emplace(temp);
    }

    for (multiset<int>::reverse_iterator rit = nums.rbegin(); rit != nums.rend(); ++rit)
        cout << *rit << " ";
}