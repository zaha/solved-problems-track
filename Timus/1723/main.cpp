#include <iostream>
#include <map>
#include <iterator>
using namespace std;

map<char, int> spellPow;

void calcPow(char spell) {
    map<char, int>::iterator it = spellPow.find(spell);

    if (it != spellPow.end())
        it -> second++;
    else
        spellPow.emplace(make_pair(spell, 1));
}

void calcMaxPow() {
    int maxPow = -1;
    char spell;

    for (pair<char, int> it : spellPow)
        if (it.second > maxPow) {
            spell = it.first;
            maxPow = it.second;
        }

    cout << spell;
}

int main() {
    string univSpell;
    cin >> univSpell;

    for (int i = 0; i < univSpell.size(); ++i) {
        char temp = univSpell[i];
        calcPow(temp);
    }

    calcMaxPow();
}
