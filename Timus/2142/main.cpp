#include <iostream>
using namespace std;

int a, b, c;
int x, y, z;

bool check() {
    if (x <= a)
        a -= x;
    else {
        x -= a;
        a = 0;
        if (x <= c)
            c -= x;
        else
            return false;
        }

    if (y <= b)
        b -= y;
    else {
        y -= b;
        b = 0;
        if (y <= c)
            c -= y;
        else
            return false;
    }

    int total = a + b + c;
    if (z <= total)
        return true;
    else
        return false;
}

int main() {
    cin >> a >> b >> c;
    cin >> x >> y >> z;

    if (check())
        cout << "It is a kind of magic";
    else
        cout << "There are no miracles in life";

    return 0;
}
