#include <iostream>
using namespace std;

int main() {
    int thicknessSheets, thicknessCover, startVol, stopVol;
    cin >> thicknessSheets >> thicknessCover >> startVol >> stopVol;

    int thicknessBook, pathLength = 0;
    thicknessBook = thicknessCover * 2 + thicknessSheets;

    if (startVol < stopVol) {
        pathLength += (stopVol - startVol - 1) * thicknessBook;
        pathLength += 2 * thicknessCover;
    } else if (startVol > stopVol) {
        pathLength += (startVol - stopVol - 1) * thicknessBook;
        pathLength += thicknessSheets * 2;
        pathLength += thicknessCover * 2;
    } else {
        pathLength = thicknessSheets;
    }

    cout << pathLength;
    return 0;
}