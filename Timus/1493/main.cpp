#include <iostream>
using namespace std;

bool lucky(int n) {
    int a = 0, b = 0;
    
    for (int i = 1; i <= 3 && n != 0; ++i) {
        a += n % 10;
        n /= 10;
    }

    for (int i = 1; i <= 3 && n != 0; ++i) {
        b += n % 10;
        n /= 10;
    }

    if (a == b)
        return true;
    else
        return false;
}

int main() {
    int num;
    cin >> num;

    int prev = num - 1;
    int next = num + 1;

    if (num == 999999 || num == 0)
        cout << "No";

    if (lucky(prev) || lucky(next))
        cout << "Yes";
    else
        cout << "No";

    return 0;
}
