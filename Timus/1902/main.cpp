#include <iostream>
#include <iomanip>
using namespace std;

int main() {
    double n, t, s;
    cin >> n >> t >> s;

    while (n--) {
        double temp;
        cin >> temp;

        cout << fixed << setprecision(6) << (temp - s + t) / 2 + s << "\n";
    }
    return 0;
}
