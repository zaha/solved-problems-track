#include <bits/stdc++.h>
using namespace std;

const int N = 1e5 + 7;
int n, s, f, r;
vector<int> roads[N];
int dist_s[N], dist_f[N], dist_r[N]; //distance from node i to s, f, r
int ans[N]; //answer for each node

void calc_dist(int root, int *dist) {
//calculate distance from root to other nodes using BFS

    queue<int> to_visit;
    bitset<N> in_queue;
    to_visit.emplace(root);
    in_queue[root] = true;
    dist[root] = 0;

    while (!to_visit.empty()) {
        for (int i : roads[to_visit.front()])
            if (!in_queue[i]) {
                dist[i] = dist[to_visit.front()] + 1;
                to_visit.emplace(i);
                in_queue[i] = true;
            }
        to_visit.pop();
    }
}

void calc_ans(int node) {
    if (node == f) {
        ans[node] = dist_r[node];
        return;
    }
    if (ans[node]) //avoid calculating same node multiple times
        return;
    int cur_ans = 0;

    for (int i : roads[node])
        if (dist_s[i] + dist_f[i] == dist_s[f]) { //use only caravn routes
            if (dist_s[i] <= dist_s[node]) //avoid going back
                continue;
            calc_ans(i);
            cur_ans = max(cur_ans, ans[i]);
        }

    ans[node] = min(cur_ans, dist_r[node]);
}

int main() {
    int m;
    cin >> n >> m;
    while (m--) {
        int a, b;
        cin >> a >> b;
        roads[a].emplace_back(b);
        roads[b].emplace_back(a);
    }
    cin >> s >> f >> r;

    calc_dist(s, dist_s);
    calc_dist(f, dist_f);
    calc_dist(r, dist_r);
    calc_ans(s);
    cout << ans[s];

    return 0;
}
