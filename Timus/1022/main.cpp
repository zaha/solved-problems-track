#include <iostream>
#include <vector>
#include <queue>
#include <set>
using namespace std;

set<int> childs[105], parents[105];
queue<int> noParents;

int main() {
    int n;
    cin >> n;

    for (int i = 1; i <= n; ++i) {
        int temp;
        cin >> temp;

        while (temp) {
            childs[i].emplace(temp);
            parents[temp].emplace(i);

            cin >> temp;
        }
    }

    for (int i = 1; i <= n; ++i) {
        if (parents[i].empty())
            noParents.emplace(i);
    }

    while (!noParents.empty()) {
        for (int child : childs[noParents.front()]) {
            parents[child].erase(parents[child].find(noParents.front()));
            if (parents[child].empty())
                noParents.emplace(child);
        }

        cout << noParents.front() << " ";
        noParents.pop();
    }
}