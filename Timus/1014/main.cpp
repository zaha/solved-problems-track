#include <iostream>
#include <deque>
using namespace std;

int main() {
    int n;
    cin >> n;
    deque<int> q;

    if (!n) {
        cout << 10;
        return 0;
    }
    if (n < 10) {
        cout << n;
        return 0;
    }
    
    for (int i = 9; i >= 2; --i)
        while (!(n % i)) {
            q.emplace_front(i);
            n /= i;
        }

    if (n > 1) {
        cout << -1;
        return 0;
    }

    while (!q.empty()) {
        cout << q.front();
        q.pop_front();
    }

    return 0;
}

//  Old code

/*
#include <iostream>
#include <set>
using namespace std;

multiset <int> digits;

void search(int n, bool &possible) {
    if (!possible)
        return;

    long long a, b;
    bool found = false;

    for (int i = 9; i >= 2; --i)
        if (n % i == 0) {
            a = i;
            found = true;
            break;
        }

    if (!found) {
        possible = false;
        return;
    }

    b = n / a;

    if (a > 9)
        search(a, possible);
    else
        digits.emplace(a);

    if (!possible)
        return;

    if (b > 9)
        search(b, possible);
    else
        digits.emplace(b);
}

int main() {
    long long n;
    cin >> n;

    if (n == 0) {
        cout << 10;
        return 0;
    }
    if (n < 10) {
        cout << n;
        return 0;
    }

    bool possible = true;
    search(n, possible);
    
    if (possible)
        for (auto it = digits.begin(); it != digits.end(); ++it)
            cout << *it;
    else
        cout << -1;

    return 0;
}
*/
