#include <iostream>
#include <string>
#include <cmath>
#define ll long long
using namespace std;

int main() {
    string num, numReversed;
    cin >> num;

    for (int i = num.size() - 1; i >= 0; --i)
        numReversed += num[i];

    char maxDigit = '0';
    for (unsigned i = 0; i < num.size(); ++i)
        if (num[i] > maxDigit)
            maxDigit = num[i];

    int base;
    if (maxDigit >= 'A')
        base = 11 + (int)(maxDigit - 'A');
    else
        base = 1 + (int)(maxDigit - '0');
    if (base < 2)
        base = 2;

    while (base <= 36) {
        ll n = 0;
        for (unsigned i = 0; i < numReversed.size(); ++i) {
            int digit;
            if (numReversed[i] >= 'A')
                digit = 10 + (int)(numReversed[i] - 'A');
            else
                digit = (int)(numReversed[i] - '0');

            n += digit;
        }

        if (!(n % (base - 1)))
            break;
        else
            ++base;
    }

    if (base <= 36)
        cout << base;
    else
        cout << "No solution.";
    return 0;
}