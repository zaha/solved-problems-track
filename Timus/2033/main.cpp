#include <iostream>
#include <string>
#include <map>
using namespace std;

int main()
{
    map<string, int> devices;
    map<string, int> prices;

    for (int i = 1; i <= 6; ++i) {
        string name, device;
        int price;
        cin >> name >> device >> price;

        map<string, int>::iterator it = devices.find(device);
        if (it != devices.end()) {
            it -> second++;

            map<string, int>::iterator it2 = prices.find(device);
            if (it2 -> second > price)
                it2 -> second = price;
        } else {
            devices.emplace(make_pair(device, 1));
            prices.emplace(make_pair(device, price));
        }
    }

    string recommendedDevice;
    int smallestPrice = 1000005, biggestRec = 0;
    map<string, int>::iterator price = prices.begin();
    for (pair<string, int> device : devices) {
        if (device.second > biggestRec || (biggestRec == device.second && price->second < smallestPrice)) {
            recommendedDevice = device.first;
            biggestRec = device.second;
            smallestPrice = price->second;
        }
        ++price;
    }

    cout << recommendedDevice;
    return 0;
}
