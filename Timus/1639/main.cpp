#include <iostream>
using namespace std;

int main() {
    int n, m;
    cin >> n >> m;

    if (n & 1 && m & 1)
        cout << "[second]=:]";
    else
        cout << "[:=[first]";

    return 0;
}
