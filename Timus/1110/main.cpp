#include <iostream>
using namespace std;

int main() {
    int n, m, y;
    cin >> n >> m >> y;
    bool found = false;

    for (int i = 0; i < m; ++i) {
        long long result = i % m;
        for (int j = 1; j < n; ++j) {
            result *= i % m;
            result %= m;
        }

        if (result == y) {
            found = true;
            cout << i << " ";
        }
    }

    if (!found)
        cout << -1;
    return 0;
}
