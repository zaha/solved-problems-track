#include <iostream>

using namespace std;

int main()
{
    int n;
    cin >> n;
    int mt[n+1][n+1];
    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= n; ++j)
            cin >> mt[i][j];

    for (int i = 1; i <= n; ++i) {
        int k = i, l = 1;
        while (k >= 1) {
            cout << mt[k][l] << " ";
            --k;
            ++l;
        }
    }

    for (int j = 2; j <= n; ++j) {
        int k = n, l = j;
        while (l <= n) {
            cout << mt[k][l] << " ";
            --k;
            ++l;
        }
    }

    return 0;
}
