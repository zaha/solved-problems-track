#include <iostream>
#include <vector>
#include <bitset>
using namespace std;

bitset<1005> studied;
vector<int> limitations[1005];

int main() {
    int n, m;
    cin >> n >> m;

    while (m--) {
        int x, y;
        cin >> x >> y;
        limitations[x].emplace_back(y);
    }

    while (n--) {
        int temp;
        cin >> temp;
        studied[temp] = true;
        
        for (unsigned i = 0; i < limitations[temp].size(); ++i) {
            if (studied[limitations[temp][i]]) {
                cout << "NO";
                return 0;
            }
        }
    }

    cout << "YES";
    return 0;
}