#include <iostream>
#include <vector>
#include <string>
using namespace std;

vector <string> users;
vector <string> passwords;
bool logged[105];

void regist() {
    string user, pass;
    cin >> user >> pass;

    for (unsigned int i = 0; i < users.size(); ++i)
        if (users[i] == user) {
            cout << "fail: user already exists\n";
            return;
        }

    users.push_back(user);
    passwords.push_back(pass);
    cout << "success: new user added\n";
}

void login() {
    string user, pass;
    cin >> user >> pass;

    int id = -1;
    for (unsigned int i = 0; i < users.size(); ++i)
        if (users[i] == user) {
            id = i;
            break;
        }

    if (id == -1) {
        cout << "fail: no such user\n";
        return;
    }

    if (passwords[id] != pass) {
        cout << "fail: incorrect password\n";
        return;
    }
    if (logged[id]) {
        cout << "fail: already logged in\n";
        return;
    }

    logged[id] = true;
    cout << "success: user logged in\n";
}

void logout() {
    string user;
    cin >> user;

    int id = -1;
    for (unsigned int i = 0; i < users.size(); ++i)
        if (users[i] == user) {
            id = i;
            break;
        }

    if (id == -1) {
        cout << "fail: no such user\n";
        return;
    }

    if (logged[id]) {
        logged[id] = false;
        cout << "success: user logged out\n";
        return;
    }

    cout << "fail: already logged out\n";
}

int main() {
    int n;
    cin >> n;

    for (int i = 1; i <= n; ++i) {
        string temp;
        cin >> temp;
        
        if (temp == "register")
            regist();

        else if (temp == "login")
            login();

        else if (temp == "logout")
            logout();
    }

    return 0;
}
