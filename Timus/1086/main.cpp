#include <iostream>
#include <vector>
#include <bitset>
using namespace std;

bitset <200000> notPrime;
vector <int> primes;

void calcPrimes() {
    for (int i = 2; i < 200000; ++i)
        if (!notPrime[i]) {
            primes.emplace_back(i);

            for (int j = i; j <= 200000; j += i)
                notPrime[j] = true;
        }
}

int main() {
    calcPrimes();

    int k;
    cin >> k;
    while (k--) {
        int temp;
        cin >> temp;

        cout << primes[temp-1] << "\n";
    }

    return 0;
}