#include <iostream>
using namespace std;

int n;
int luckyes = 1; // set this to one to cover the test case with only digits of 0
int digits[15];

bool increase(int level) {
    if (!level)
        return false;
    bool possible = true;

    if (digits[level] == 9) {
        digits[level] = 0;
        possible = increase(level - 1);
    } else
        ++digits[level];

    return possible;
}

void brute() {
    while (increase(n)) {
        int sum1 = 0, sum2 = 0;

        int i = 1;
        for (; i <= n / 2; ++i)
            sum1 += digits[i];
        for (; i <= n; ++i)
            sum2 += digits[i];

        if (sum1 == sum2)
            ++luckyes;
    }
}

int main() {
    cin >> n;
    brute();

    cout << luckyes;
    return 0;
}