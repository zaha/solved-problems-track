#include <iostream>
#include <queue>
#include <set>
using namespace std;

int main() {
    queue<int> nums;
    multiset<int> numsSorted;
    int m;
    cin >> m;

    if (m == 2) {
        bool useSecond = false;
        int first, second;
        cin >> first >> second;
        while (first != -1 && second  != -1) {
            cout << max(first, second) << "\n";

            if (useSecond)
                cin >> second;
            else
                cin >> first;
            useSecond = !useSecond;
        }

        return 0;
    }

    while ((int)nums.size() < m) {
        int temp;
        cin >> temp;
        nums.emplace(temp);
        numsSorted.emplace(temp);
    }

    while (nums.back() != -1) {
        multiset<int>::reverse_iterator maxNum = numsSorted.rend();
        ++maxNum;
        cout << *maxNum << "\n";
        numsSorted.erase(numsSorted.lower_bound(nums.front()));
        nums.pop();
        int temp;
        cin >> temp;
        nums.emplace(temp);
        numsSorted.emplace(temp);
    }

    return 0;
}
