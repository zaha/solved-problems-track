#include <iostream>
#include <string>
using namespace std;

string msg;

bool check(int &i, int &j) {
    if (i < 0 || j >= msg.size() || msg[i] != msg[j])
        return false;

    while (i && j < msg.size() - 1 && msg[i - 1] == msg[j + 1]) {
        --i;
        ++j;
    }

    return true;
    
}

int main() {
    int maxLen = 1, startingPoint = 0;
    cin >> msg;

    for (unsigned i = 0; i < msg.size(); ++i) {
        int start = i, stop = i + 1;
        if (check(start, stop))
            if (stop - start + 1 > maxLen) {
                maxLen = stop - start + 1;
                startingPoint = start;
            }

        start = i - 1, stop = i + 1;
        if (check(start, stop))
            if (stop - start + 1 > maxLen) {
                maxLen =stop - start + 1;
                startingPoint = start;
            }
    }

    for (int i = 0; i < maxLen; ++i)
        cout << msg[startingPoint + i];
    return 0;
}