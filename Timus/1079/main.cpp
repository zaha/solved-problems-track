#include <iostream>
#include <vector>
using namespace std;

int main() {
    int n;
    cin >> n;

    while (n) {
        int maxim = 1;
        vector <int> numbers; 
        numbers.emplace_back(0);
        numbers.emplace_back(1);

        for (int pos = 2; pos <= n; ++pos) {
            if (pos & 1)
                numbers.emplace_back(numbers[(pos - 1) / 2] + numbers[((pos - 1) / 2) + 1]);
            else
                numbers.emplace_back(numbers[pos / 2]);

            if (numbers[pos] > maxim)
                maxim = numbers[pos];
        }

        cout << maxim << "\n";
        cin >> n;
    }
}