#include <iostream>
#include <set>
using namespace std;

multiset <int> skins;

int main() {
    int n;
    cin >> n;

    int m = 0;
    for (int i = 1; i <= n; ++i) {
        int temp;
        cin >> temp;
        skins.emplace(temp);

        if (temp > m)
            m = temp;
    }

    int sheets = m;
    for (auto i : skins)
        sheets += i;

    cout << sheets;
    return 0;
}