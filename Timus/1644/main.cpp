#include <iostream>
#include <string>
using namespace std;

int main()
{
    int n;
    cin >> n;

    int minSat = 10, maxHun = 2;
    while (n--) {
        int num;
        string status;
        cin >> num >> status;

        if (status == "hungry" && num > maxHun)
            maxHun = num;
        if (status == "satisfied" && num < minSat)
            minSat = num;
    }

    if (minSat <= maxHun)
        cout << "Inconsistent";
    else
        cout << minSat;
    return 0;
}
