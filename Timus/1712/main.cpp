#include <iostream>
#include <bitset>
#include <string>
using namespace std;

bitset<5> gridPos[5];
char letters[5][5];

void rotateGrid() {
    bitset<5> temp[5];
    for (int i = 1; i <= 4; ++i)
        for (int j = 1; j <= 4; ++j)
                temp[j][4-i+1] = (gridPos[i][j]) ? true : false;

    for (int i = 1; i <= 4; ++i)
        for (int j = 1; j <= 4; ++j)
            gridPos[i][j] = temp[i][j];
}

int main() {
    for (int i = 1; i <= 4; ++i) {
        string temp;
        cin >> temp;

        for (int j = 0; j < 4; ++j)
            if (temp[j] == 'X')
                gridPos[i][j+1] = true;
    }

    for (int i = 1; i <= 4; ++i) {
        string temp;
        cin >> temp;

        for (int j = 0; j < 4; ++j)
            letters[i][j+1] = temp[j];
    }

    for (int i = 1; i <= 4; ++i) {
        for (int x = 1; x <= 4; ++x)
            for (int y = 1; y <= 4; ++y)
                if (gridPos[x][y])
                    cout << letters[x][y];
        rotateGrid();
    }
}