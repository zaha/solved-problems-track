#include <iostream>
#include <bitset>
using namespace std;

int n;
bitset <25> onSecondList;
long long weights[25];
long long minDif;
bool started = false;

void calcDif() {
    long long list1 = 0, list2 = 0;
    for (int i = 1; i <= n; ++i)
        if (onSecondList[i])
            list2 += weights[i];
        else
            list1 += weights[i];

    int dif = max(list1, list2) - min(list1, list2);
    if (!started)
        minDif = dif;
    else
        if (dif < minDif)
            minDif = dif;
}

bool nextCase(int level) {
    if (!level)
        return false;
    bool possible = true;

    if (onSecondList[level]) {
        onSecondList[level] = false;
        possible = nextCase(level - 1);
    } else
        onSecondList[level] = true;

    calcDif();
    return possible;
}

int main() {
    cin >> n;

    for (int i = 1; i <= n; ++i) {
        long long temp;
        cin >> temp;
        weights[i] = temp;
    }

    calcDif(); // for test case with all weights in first list (just in case)
    started = true;
    while(nextCase(n));

    cout << minDif;
    return 0;
}