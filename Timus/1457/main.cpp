#include <iostream>
#include <iomanip>
using namespace std;

int main() {
    int n;
    double distance = 0.0;
    cin >> n;

    for (int i = 1; i <= n; ++i) {
        int temp;
        cin >> temp;
        distance += temp;
    }

    cout << fixed << setprecision(6) << distance / n;
    return 0;
}
