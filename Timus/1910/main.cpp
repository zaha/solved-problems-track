#include <iostream>
using namespace std;

int main() {
    int n;
    cin >> n;
    int v[n+5];
    for (int i = 1; i <= n; ++i)
        cin >> v[i];

    int max = 0, pos = 0;
    for (int i = 1; i <= n - 2; ++i) {
        int sum = v[i] + v[i+1] + v[i+2];
        
        if (sum > max) {
            max = sum;
            pos = i + 1;
        }
    }

    cout << max << " " << pos;
    return 0;
}
