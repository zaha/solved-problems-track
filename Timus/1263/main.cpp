#include <iostream>
#include <iomanip>
using namespace std;

int votes[10005];

int main() {
    int candidates, electors;
    cin >> candidates >> electors;

    for (int i = 1; i <= electors; ++i) {
        int temp;
        cin >> temp;
        ++votes[temp];
    }

    for (int i = 1; i <= candidates; ++i)
        cout << setprecision(2) << fixed << (double(votes[i]) * 100.00) / electors << "%\n";

    return 0;
}
