#include <iostream>
#include <queue>
using namespace std;

int main() {
    int disks[5], petals[4];
    for (int i = 1; i <= 4; ++i)
        cin >> disks[i];
    for (int i = 1; i <= 3; ++i)
        cin >> petals[i];

    queue<int> wrongs; // wrong petals
    if ((disks[1] + disks[2] + disks[4]) % 2 != petals[3])
        wrongs.emplace(3);
    if ((disks[1] + disks[3] + disks[4]) % 2 != petals[2])
        wrongs.emplace(2);
    if ((disks[2] + disks[3] + disks[4]) % 2 != petals[1])
        wrongs.emplace(1);

    if (wrongs.size() == 3) {
        if (disks[4])
            disks[4] = 0;
        else
            disks[4] = 1;
    } else if (wrongs.size() == 2) {
        int a = wrongs.front();
        wrongs.pop();
        int b = wrongs.front();
        if (a == 3 && b == 2) {
            if (disks[1])
                disks[1] = 0;
            else
                disks[1] = 1;
        }
        if (a == 3 && b == 1) {
            if (disks[2])
                disks[2] = 0;
            else
                disks[2] = 1;
        }
        if (a == 2 && b == 1) {
            if (disks[3])
                disks[3] = 0;
            else
                disks[3] = 1;
        }
    } else if (wrongs.size()) {
        if (petals[wrongs.front()])
            petals[wrongs.front()] = 0;
        else
            petals[wrongs.front()] = 1;
    }

    for (int i = 1; i <= 4; ++i)
        cout << disks[i] << " ";
    for (int i = 1; i <= 3; ++i)
        cout << petals[i] << " ";
    return 0;
}