#include <iostream>
#include <map>
#include <iterator>
using namespace std;

map<int, int> wheels;

int calcPairs() {
    int pairs = 0;
    for (pair<int, int> it : wheels)
        pairs += it.second / 4;
    return pairs;
}

int main() {
    int n;
    cin >> n;

    for (int i = 1; i <= n; ++i) {
        int wheel;
        cin >> wheel;

        map<int, int>::iterator pos = wheels.find(wheel);
        if (pos != wheels.end())
            pos -> second++;
        else
            wheels.emplace(make_pair(wheel, 1));
    }

    cout << calcPairs();
    return 0;
}
