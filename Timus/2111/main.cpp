#include <iostream>
#include <set>
using namespace std;

int main() {
    long long n, tons = 0;
    multiset<long long> cities;
    cin >> n;

    for (int i = 1; i <= n; ++i) {
        long long temp;
        cin >> temp;

        cities.emplace(temp);
        tons += temp;
    }

    long long toPay = 0;
    for (auto it = cities.begin(); it != cities.end(); ++it) {
        long long km = *it;
        toPay += tons * km;

        tons -= km;
        toPay += km * tons;
    }

    cout << toPay;
    return 0;
}