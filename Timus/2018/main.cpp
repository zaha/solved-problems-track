#include <bits/stdc++.h>
#define ll long long
using namespace std;

const ll MOD = 1e9 + 7, N = 5e5;
ll songs[N][3];

int main() {
    ll a, b, n;
    cin >> n >> a >> b;
    songs[0][1] = songs[0][2] = 1;

    for (int i = 0; i < n; ++i) {
        for (int j = 1; j <= a && i + j <= n; ++j)
            songs[i + j][1] = (songs[i + j][1] + songs[i][2]) % MOD;
        for (int j = 1; j <= b && i + j <= n; ++j)
            songs[i + j][2] = (songs[i + j][2] + songs[i][1]) % MOD;
    }

    cout << (songs[n][1] + songs[n][2]) % MOD;
    return 0;
}
