#include <bits/stdc++.h>
using namespace std;

int lab[35][35]; //labyrinth
bitset<35> visited[35];
int n, walls = -4; //-4 to avoid counting entrances as walls

         //row, col
void dfs(int i, int j) {
    visited[i][j] = true;

    //up
    if (i == 1 || !lab[i - 1][j]) ++walls;
    else if (!visited[i - 1][j]) dfs(i - 1, j);

    //right
    if (j == n || !lab[i][j + 1]) ++walls;
    else if (!visited[i][j + 1]) dfs(i, j + 1);

    //down
    if (i == n || !lab[i + 1][j]) ++walls;
    else if (!visited[i + 1][j]) dfs(i + 1, j);

    //left
    if (j == 1 || !lab[i][j - 1]) ++walls;
    else if (!visited[i][j - 1]) dfs(i, j - 1);
}

int main() {
    cin >> n;
    for (int i = 1; i <= n; ++i) {
        string row;
        cin >> row;
        for (int j = 0; j < n; ++j)
            if (row[j] == '.') ++lab[i][j + 1];
    }

    dfs(1, 1);
    if (!visited[n][n]) dfs(n, n);
    cout << walls * 9;
}
