#include <iostream>
#include <cmath>
using namespace std;

bool isPerfSquare(long long n) {
    long long root = round(sqrt(n));
    return n == root * root;
}

int main() {
    int n;
    cin >> n;

    for (int i = 1; i <= n; ++i) {
        long long pos;
        cin >> pos;

        long long x = pos - 1;

        if (isPerfSquare(x * 8 + 1))
            cout << 1 << ' ';
        else
            cout << 0 << ' ';
    }

    return 0;
}
