#include <iostream>
#include <vector>
using namespace std;

int main() {
    int distances[6][6];
    for (int i = 1; i <= 5; ++i)
        for (int j = 1; j <= 5; ++j)
            cin >> distances[i][j];

    vector<int> path = {1, 2, 3, 4, 5 };
    int minDist = distances[1][2] + distances[2][3] + distances[3][4] + distances[4][5];
    int temp;

    temp = distances[1][3] + distances[3][2] + distances[2][4] + distances[4][5];
    if (temp < minDist) {
        path = {1, 3, 2, 4, 5};
        minDist = temp;
    }
    temp = distances[1][3] + distances[3][4] + distances[4][2] + distances[2][5];
    if (temp < minDist) {
        path = {1, 3, 4, 2, 5};
        minDist = temp;
    }
    temp = distances[1][4] + distances[4][3] + distances[3][2] + distances[2][5];
    if (temp < minDist) {
        path = {1, 4, 3, 2, 5};
        minDist = temp;
    }

    cout << minDist << "\n";
    for (int i : path)
        cout << i << " ";
    return 0;
}