#include <iostream>
#include <cmath>
#include <string>
#include <deque>
#define ll long long
using namespace std;

int main() {
    string type;
    ll number;
    cin >> type >> number;

    deque <ll> nums;
    for (ll i = 3; i >= 0; --i) {
        ll temp = number / pow(256, i);
        nums.emplace_back(temp);
        number -= temp * pow(256, i);
    }

    ll result = 0;
    if (type == "GOOD")
        for (ll i = 0; i <= 3; ++i)
            result += nums[i] * pow(256, i);
    else
        for (ll i = 3; i >= 0; --i)
            result += nums[i] * pow(256, i);
           
    cout << result;
    return 0;
}