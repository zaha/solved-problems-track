#include <iostream>
#include <string>
using namespace std;

int main() {
    string manuscript, name = "Sandro";
    cin >> manuscript;

    int minToPay = name.size() * 10;
    for (int i = 0; i <= manuscript.size() - name.size(); ++i) {
        int toPay = 0;
        for (int j = 0; j < name.size(); ++j) {
            if (name[j] >= 'a' && manuscript[i + j] <= 'Z') {
                if (name[j] - 'a' != manuscript[i + j] - 'A')
                    toPay += 10;
                else
                    toPay += 5;
            } else if (name[j] <= 'Z' && manuscript[i + j] >= 'a') {
                if (name[j] - 'A' != manuscript[i + j] - 'a')
                    toPay += 10;
                else
                    toPay += 5;
            } else {
                if (name[j] != manuscript[i + j])
                    toPay += 5;
            }
        }

        if (toPay < minToPay)
            minToPay = toPay;
    }

    cout << minToPay;
    return 0;
}
