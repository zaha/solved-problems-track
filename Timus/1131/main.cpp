#include <iostream>
using namespace std;

int main()
{
    int n, k;
    cin >> n >> k;

    int counter = 0, available = 1;
    while (available <= k && available < n) {
        ++counter;
        available *= 2;
    }
    counter += max(0, (n-available + k-1)/k);

    cout << counter;
    return 0;
}
