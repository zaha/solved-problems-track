#include <iostream>
using namespace std;

int main() {
    int a, b, nights = 0;
    cin >> a >> b;
    if ((a % 2 == 0 || b % 2 != 0) && b != 0) {
        cout << "yes";
    } else {
        cout << "no";
    }
    return 0;
}