#include <iostream>
#include <string>
using namespace std;

int main() {
    string n;
    cin >> n;

    int rest = 0;
    for (int i = 0; n[i]; ++i)
        rest = (rest * 10 + (n[i] - '0')) % 7;

    cout << rest;
    return 0;
}
