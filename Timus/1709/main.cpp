#include <bits/stdc++.h>
#define ll long long
using namespace std;

int graph[105][105], ans[105][105];
ll n, d, a, cnt;
bitset<105> visited;

void connect(int node) {
    graph[1][node] = 1;
    graph[node][1] = 1;
    ans[node][1] = 1;
    ans[1][node] = 1;
    cnt += a;
}

void remEdge(int x, int y) {
    graph[x][y] = 0;
    graph[y][x] = 0;
    ans[x][y] = 2;
    ans[y][x] = 2;
    cnt += d;
}

void dfs(int node, int parent = 0) {
    visited[node] = true;
    for (int i = 1; i <= n; ++i)
        if (graph[node][i] && parent != i) {
            if (visited[i])
                remEdge(node, i);
            else
                dfs(i, node);
        }
}

int main() {
    cin >> n >> d >> a;

    for (int i = 1; i <= n; ++i) {
        string line;
        cin >> line;
        for (int j = 0; j < n; ++j)
            if (line[j] == '1')
                graph[i][j+1] = 1;
    }

    dfs(1);
    for (int i = 1; i <= n; ++i)
        if (!visited[i]) {
            dfs(i);
            connect(i);
        }

    cout << cnt << "\n";
    for (int i = 1; i <= n; ++i) {
        for (int j = 1; j <= n; ++ j) {
            if (ans[i][j] == 1)
                cout << 'a';
            else if (ans[i][j] == 2)
                cout << 'd';
            else
                cout << '0';
        }
        cout << "\n";
    }
}
