#include <iostream>
using namespace std;

int baragain(int &a, int b, int &c, int d) {
    if (c < a)
        return a;
    
    while (true) {
        if (a + b > c)
            return c;
        a += b;
        
        if (c - d < a)
            return a;
        c -= d;
    }
}

int main() {
    int a, b, c, d;
    cin >> a >> b >> c >> d;

    cout << baragain(a, b, c, d);
    return 0;
}
