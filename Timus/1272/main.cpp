#include <bits/stdc++.h>
using namespace std;
const int maxN = 1e5 + 5;

int n, k, m, cnt;
vector<int> tunels[maxN], bridges[maxN];
bitset<maxN> visited;

void dfs(int node) {
    visited[node] = true;
    for (unsigned i = 0; i < tunels[node].size(); ++i) {
        int neighbour = tunels[node][i];
        if (!visited[neighbour])
            dfs(neighbour);
    }
}

int main() {
    cin >> n >> k >> m;

    for (int i = 1; i <= k; ++i) {
        int x, y;
        cin >> x >> y;
        tunels[x].emplace_back(y);
        tunels[y].emplace_back(x);
    }

    for (int i = 1; i <= m; ++i) {
        int x, y;
        cin >> x >> y;
        bridges[x].emplace_back(y);
        bridges[y].emplace_back(x);
    }

    for (int i = 1; i <= n; ++i)
        if (!visited[i]) {
            ++cnt;
            dfs(i);
        }

    cout << cnt - 1;
    return 0;
}
