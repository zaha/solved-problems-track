#include <iostream>
#include <cmath>
#include <queue>
#include <iomanip>
using namespace std;

int main() {
    priority_queue<double> nums;
    int n;
    cin >> n;

    while (n--) {
        double temp;
        cin >> temp;
        nums.emplace(temp);
    }

    while (nums.size() > 1) {
        double temp1, temp2;
        temp1 = nums.top();
        nums.pop();
        temp2 = nums.top();
        nums.pop();

        nums.emplace(2 * sqrt(temp1 * temp2));
    }
    
    cout << fixed << setprecision(2) << nums.top();
    return 0;
}