#include <bits/stdc++.h>
using namespace std;

int prices[105][105], n, k, totalPrice;
bitset<105> powered;
priority_queue<pair<int, int>, vector<pair<int, int>>, greater<pair<int, int>>> cities;

void addToQueue(int node) {
    powered[node] = true;
    for (int i = 1; i <= n; ++i)
        if (!powered[i])
            cities.emplace(make_pair(prices[node][i], i));
}

void solve() {
    while (!cities.empty() && powered[cities.top().second])
        cities.pop();
    if (cities.empty())
        return;

    pair<int, int> nextCity = cities.top();
    addToQueue(nextCity.second);
    totalPrice += nextCity.first;
}

int main() {
    cin >> n >> k;

    queue<int> initialPower;
    for (int i = 1; i <= k; ++i) {
        int temp;
        cin >> temp;
        powered[temp] = true;
        initialPower.emplace(temp);
    }

    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= n; ++j)
            cin >> prices[i][j];

    while (!initialPower.empty()) {
        addToQueue(initialPower.front());
        initialPower.pop();
    }

    while (!cities.empty())
        solve();

    cout << totalPrice;
    return 0;
}
