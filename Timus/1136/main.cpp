#include <iostream>
#include <stack>
#include <utility>
using namespace std;

stack<int> reversed;
int parents[65540];
pair<int, int> childrens[65540];

void makeGraph() {
    int current = reversed.top();
    reversed.pop();
    while (!reversed.empty()) {
        int temp = current;
        while (reversed.top() < parents[temp] && parents[temp]) {
            temp = parents[temp];
            if (!childrens[temp].first)
                current = temp;
        }
        parents[reversed.top()] = current;

        if (reversed.top() < current)
            childrens[current].first = reversed.top();
        else
            childrens[current].second = reversed.top();
        current = reversed.top();
        reversed.pop();
    }
}

void printAnswer(int current) {
    if (childrens[current].second)
        printAnswer(childrens[current].second);
    if (childrens[current].first)
        printAnswer(childrens[current].first);
    cout << current << "\n";
}

int main() {
    int n;
    cin >> n;

    int temp;
    while (n--) {
        cin >> temp;
        reversed.emplace(temp);
    }

    makeGraph();
    printAnswer(temp);
    return 0;
}
