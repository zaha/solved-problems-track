#include <iostream>
using namespace std;

int main() {
    int n, k;
    cin >> n >> k;

    int unused = 0, survivors = 0;

    for (int i = 1; i <= n; ++i) {
        int temp;
        cin >> temp;

        if (temp < k)
            survivors += k - temp;

        if (k < temp)
            unused += temp - k;
    }

    cout << unused << " " << survivors;
    return 0;
}
