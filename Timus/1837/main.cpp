#include <iostream>
#include <vector>
#include <map>
#include <queue>
#include <string>
#include <iterator>
#include <utility>
using namespace std;

map <string, int> participants;
vector <int> graph[1005];
int dist[1005]; // distance
queue <int> unvisited;
bool isenbaev = false; // is Isenbaev present?

void calcDistance() {
    unvisited.emplace(0);

    while (!unvisited.empty()) {
        int participant = unvisited.front();
        unvisited.pop();

        for (int i = 0; i < (int)graph[participant].size(); ++i) {
            int neighbor = graph[participant][i];
            if (!dist[neighbor] && neighbor) {
                unvisited.emplace(neighbor);
                dist[neighbor] = dist[participant] + 1;
            }
        }
    }
}

void addEdge(int a, int b) {
    graph[a].emplace_back(b);
    graph[b].emplace_back(a);
}

void makeGraph(vector <int> members) {
    addEdge(members[0], members[1]);
    addEdge(members[0], members[2]);
    addEdge(members[1], members[2]);
}

int main() {
    int n;
    cin >> n;
    participants.emplace(make_pair((string)"Isenbaev", 0));

    for (int team = 1; team <= n; ++team) {
        vector <int> members;
        for (int i = 1; i <= 3; ++i) {
            string member;
            cin >> member;
            if (member == "Isenbaev")
                isenbaev = true;

            map<string, int>::iterator participant = participants.find(member);
            if (participant != participants.end())
                members.emplace_back(participant -> second);
            else {
                participants.emplace(make_pair(member, participants.size()));
                members.emplace_back(participants.size() - 1);
            }
        }
        makeGraph(members);
    }

    calcDistance();
    for (pair <string, int> participant : participants)
        if (!participant.second) {
            if (isenbaev)
                cout << "Isenbaev 0\n";
        } else if (!dist[participant.second])
            cout << participant.first << " undefined\n";
        else
            cout << participant.first << " " << dist[participant.second] << "\n";
}
