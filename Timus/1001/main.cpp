#include <iostream>
#include <iomanip>
#include <cmath>
#include <stack>
using namespace std;

int main() {
    double temp;
    stack <double> numbers;
    
    while (cin >> temp) 
        numbers.push(temp);

    while (!numbers.empty()) {
        cout << setprecision(4) << fixed << sqrt(double(numbers.top())) << "\n";
        numbers.pop();
    }

    return 0;
}
