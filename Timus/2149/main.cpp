#include <iostream>
#include <string>
using namespace std;

int main() {
    int n;
    string heads;
    cin >> n >> heads;

    // we need only first line from input, which contains heads direction
    string temp;
    cin >> temp >> temp;

    int leftRight = 0, leftLeft = 0;
    int rightRight = 0, rightLeft = 0; // number of pigeons from second half looking in each direction
    int oddLeft = 0, oddRight = 0;
    int evenLeft = 0, evenRight = 0; // number of pigeons from even positions looking in each direction

    int counter = 1, pos = 0;
    while (counter <= n) {
        if (heads[pos] == '<') {
            if (counter & 1) // if it's odd
                oddLeft++;
            else
                evenLeft++;

            if (counter <= n / 2)
                leftLeft++;
            else
                rightLeft++;

            ++counter;
        }

        if (heads[pos] == '>') {
            if (counter & 1)
                oddRight++;
            else
                evenRight++;

            if (counter <= n / 2)
                leftRight++;
            else
                rightRight++;

            ++counter;
        }

        ++pos;
    }

    cout << min(
                min(leftRight + rightLeft, leftLeft + rightRight),
                min(oddRight + evenLeft, oddLeft + evenRight)
            );

    return 0;
}