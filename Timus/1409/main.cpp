#include <iostream>
using namespace std;

int main() {
    int l, h, cans;
    cin >> l >> h;
    cans = l + h - 1;
    cout << cans - l << " " << cans - h;
    return 0;
}