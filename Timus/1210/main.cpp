#include <bits/stdc++.h>
using namespace std;

char tmp;
int fee[35][35];

int main() {
    for (int i = 1; i < 35; ++i)
        for (int j = 0; j < 35; ++j)
            fee[i][j] = 10000000;

    int n;
    cin >> n;

    for (int level = 1; level <= n; ++level) {
        int planets;
        cin >> planets;

        for (int planet = 1; planet <= planets; ++planet) {
            int num;
            cin >> num;

            while (num) {
                int f;
                cin >> f;
                fee[level][planet] = min(f + fee[level-1][num], fee[level][planet]);
                cin >> num;
            }
        }

        if (level < n)
            cin >> tmp;
    }

    int minCost = 10000000;
    for (int i = 1; fee[n][i] != 10000000; ++i)
        minCost = min(minCost, fee[n][i]);
    cout << minCost;
}
