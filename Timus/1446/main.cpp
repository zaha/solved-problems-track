#include <iostream>
#include <string>
#include <vector>
using namespace std;

int main()
{
    int n;
    cin >> n;

    string temp;
    getline(cin, temp); // move to next line

    vector<string> slytherin, hufflepuff, gryffindor, ravenclaw;
    while (n--) {
        string name, house;
        getline(cin, name);
        getline(cin, house);

        if (house == "Slytherin")
            slytherin.emplace_back(name);
        else if (house == "Hufflepuff")
            hufflepuff.emplace_back(name);
        else if (house == "Gryffindor")
            gryffindor.emplace_back(name);
        else
            ravenclaw.emplace_back(name);
    }

    cout << "Slytherin:\n";
    for (string name : slytherin)
        cout << name << "\n";
    cout << "\nHufflepuff:\n";
    for (string name : hufflepuff)
        cout << name << "\n";
    cout << "\nGryffindor:\n";
    for (string name : gryffindor)
        cout << name << "\n";
    cout << "\nRavenclaw:\n";
    for (string name : ravenclaw)
        cout << name << "\n";

    return 0;
}
