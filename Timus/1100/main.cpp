#include <iostream>
#include <vector>
#include <queue>
using namespace std;

vector <int> standings[105];
bool present[105];

int main() {
    int n;
    cin >> n;

    for (int i = 1; i <= n; ++i) {
        int id, m;
        cin >> id >> m;

        standings[m].push_back(id);
        present[m] = true;
    }

    for (int i = 105; i >= 0; --i)
        if (present[i])
            for (unsigned int j = 0; j < standings[i].size(); ++j)
                cout << standings[i][j] << " " << i << "\n";
    
    return 0;
}
