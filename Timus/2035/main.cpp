#include <iostream>
using namespace std;

int main() {
    long long x, y, c;
    cin >> x >> y >> c;

    if (x + y < c)
        cout << "Impossible";
    else if (y >= c)
        cout << "0 " << c;
    else
        cout << c - y << " " << y;

    return 0;
}