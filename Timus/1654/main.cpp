#include <iostream>
#include <string>
#include <deque>
using namespace std;

int main() {
    string encrypted;
    deque <char> message;
    cin >> encrypted;

    for (int i = 0; i < (int)encrypted.size(); ++i) {
        if (message.empty()) {
            message.emplace_back(encrypted[i]);
            continue;
        }

        if (encrypted[i] == message.back())
            message.pop_back();
        else
            message.emplace_back(encrypted[i]);
    }

    while (!message.empty()) {
        cout << message.front();
        message.pop_front();
    }
    return 0;
}
