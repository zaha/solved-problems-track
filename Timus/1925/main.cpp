#include <iostream>
using namespace std;

int main() {
    int n, sumIn = 0, sumOut;
    cin >> n >> sumOut;

    for (int i = 1; i <= n; ++i) {
        int a, b;
        cin >> a >> b;
        sumOut += a;
        sumIn += b;
    }

    if (sumIn <= sumOut - ((n+1)*2))
        cout << sumOut - ((n+1)*2) - sumIn;
    else
        cout << "Big Bang!";
}
