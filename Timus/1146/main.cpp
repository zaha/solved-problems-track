#include <iostream>
using namespace std;

int n;
int arr[103][103], sums[103][103];

int calcMaxSum() {
    int maxSum = arr[1][1];
    
    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= n; ++j)
            for (int x = 1; x <= i; ++x)
                for (int y = 1; y <= j; ++y)
                    if (sums[i][j] - sums[x-1][j] - sums[i][y-1] + sums[x-1][y-1] > maxSum)
                        maxSum = sums[i][j] - sums[x-1][j] - sums[i][y-1] + sums[x-1][y-1];

    return maxSum;
}

int main() {
    cin >> n;
    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= n; ++j)
            cin >> arr[i][j];

    for (int i = 1; i <= n; ++i)
        for (int j = 1; j <= n; ++j)
            sums[i][j] = arr[i][j] + (sums[i-1][j] + sums[i][j-1] - sums[i-1][j-1]);

    cout << calcMaxSum();
    return 0;
}