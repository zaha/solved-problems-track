#include <iostream>
using namespace std;

int main() {
    int n, m, count = 2;
    cin >> n >> m;
    
    for (int i = 1; i <= n; ++i)
        cout << i << " ";
    cout << "\n";

    for (int i = 1; i <= m; ++i)
        cout << 100 * i << " ";
    return 0;
}