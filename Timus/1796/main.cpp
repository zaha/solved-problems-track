#include <iostream>
#include <stack>
using namespace std;

int main()
{
    int tickets[7], totalGiven = 0, smallest = 0, price, lastTickPrice = 5;
    for (int i = 1; i <= 6; ++i) {
        cin >> tickets[i];
        int tickPrice = lastTickPrice *= (i & 1) ? 2:5;
        lastTickPrice = tickPrice;

        if (!smallest && tickets[i])
            smallest = tickPrice;
        totalGiven += tickets[i] * tickPrice;
    }
    cin >> price;

    stack<int> childs;
    childs.emplace(totalGiven/price);
    while ((childs.top() - 1) * price > totalGiven - smallest)
        childs.emplace(childs.top() - 1);

    cout << childs.size() << "\n";
    while (!childs.empty()) {
        cout << childs.top() << " ";
        childs.pop();
    }

    return 0;
}
