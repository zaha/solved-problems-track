#include <iostream>
#include <string>
using namespace std;

int main() {
    int h, w, n;
    cin >> h >> w >> n;

    int curSym = 0, curLin = 1, curPg = 1;
    for (int i = 1; i <= n; ++i) {
        string temp;
        cin >> temp;

        curSym += temp.size() + 1;
        if (curSym > w+1) {
            curSym = temp.size() + 1;
            ++curLin;
            if (curLin > h) {
                ++curPg;
                curLin = 1;
            }
        }
    }

    cout << curPg;
    return 0;
}
