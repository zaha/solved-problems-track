#include <bits/stdc++.h>
#define ll long long
using namespace std;

ll dp[10][90];

int main() {
    int s;
    cin >> s;

    if (s == 1) { //edge case
        cout << 10;
        return 0;
    }
    
    for (int i = 1; i <= 9; ++i)
        dp[1][i] = 1;

    for (int i = 2; i <= 9; ++i)
        for (int j = 1; dp[i - 1][j]; ++j)
            for (int k = 0; k <= 9; ++k)
                dp[i][j + k] += dp[i - 1][j];

    ll ans = 0;
    for (int i = 1; i <= 9; ++i)
        ans += dp[i][s];
    cout << ans;
    return 0;
}
