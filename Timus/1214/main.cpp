#include <iostream>
using namespace std;

int main() {
    long x, y;
    cin >> x >> y;

    if (x > 0 && y > 0) {
        if (((x & 1) && (y & 1)) || (!(x & 1) && !(y & 1)))
            cout << x << " " << y;
        else
            cout << y << " " << x;
    } else {
        cout << x << " " << y;
    }

    return 0;
}