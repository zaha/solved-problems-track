#include <iostream>
using namespace std;

int sums[10005];

int main()
{
    int n;
    cin >> n;
    for (int i = 1; i <= n; ++i) {
        int temp;
        cin >> temp;
        sums[i] = sums[i-1] + temp;
    }

    int t;
    cin >> t;
    while (t--) {
        int i, j;
        cin >> i >> j;
        cout << sums[j] - sums[i-1] << "\n";
    }

    return 0;
}
