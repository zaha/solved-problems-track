#include <iostream>
#include <string>
using namespace std;

int main() {
    int n;
    string nums[4] = {"16", "06", "68", "88"};
    cin >> n;

    if (n > 4)
        cout << "Glupenky Pierre";
    else
        for (int i = 0; i <n; ++i)
            cout << nums[i] << " ";
    return 0;
}