#include <bits/stdc++.h>
#define red 1
#define blue 2
using namespace std;

int color[100];
vector<int> edges[100];

bool set_colors() {
    queue<int> to_visit;
    to_visit.emplace(1);
    color[1] = red;

    while (!to_visit.empty()) {
        int country = to_visit.front();
        to_visit.pop();

        for (int i : edges[country]) {
            if (color[i] == color[country])
                return false;

            if (!color[i]) {
                color[i] = color[country] == red ? blue : red;
                to_visit.emplace(i);
            }
        }
    }

    return true;
}

int main() {
    int n;
    cin >> n;
    for (int i = 1; i <= n; ++i) {
        int j;
        cin >> j;

        while (j) {
            edges[i].emplace_back(j);
            edges[j].emplace_back(i);
            cin >> j;
        }
    }

    if (set_colors()) {
        for (int i = 1; i <= n; ++i)
            cout << color[i] - 1;
    } else
        cout << -1;
    return 0;
}
