#include <iostream>
#include <string>
using namespace std;

int main() {
    string line;
    bool newSentence = true;
    while (getline(cin, line)) {
        for (int i = 0; i < (int)line.size(); ++i) {
            if (line[i] == '.' || line[i] == '!' || line[i] == '?')
                newSentence = true;

            if (line[i] >= 'A' && line[i] <= 'Z') {
                if (newSentence) {
                    cout << line[i];
                    newSentence = false;
                } else
                    cout << (char)(line[i] + ('a' - 'A'));
            } else
                cout << line[i];
        }
       
        cout << "\n";
    }
    return 0;
}
