#include <iostream>
#include <string>
#include <vector>
using namespace std;

vector <string> submits;
vector <string> reported;

bool submited(string user) {
    for (unsigned int i = 0; i < submits.size(); ++i)
        if (submits[i] == user)
            return true;

    submits.push_back(user);
    return false;
}

void report(string user) {
    for (unsigned int i = 0; i < reported.size(); ++i)
        if (reported[i] == user)
            return;

    reported.push_back(user);
    cout << user << "\n";
}

int main() {
    int n;
    cin >> n;

    for (int i = 1; i <= n; ++i) {
        string user;
        cin >> user;

        if (submited(user))
            report(user);
    }

    return 0;
}
