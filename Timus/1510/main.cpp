#include <iostream>
#include <map>
using namespace std;

map<int, int> vals;

int main() {
    int n;
    cin.tie(0);
    ios::sync_with_stdio(0);
    cin >> n;

    for (int i = 1; i <= n; ++i) {
        int temp;
        cin >> temp;
        map<int, int>::iterator it = vals.find(temp);
        if (it != vals.end())
            it -> second++;
        else
            vals.emplace(make_pair(temp, 1));
    }

    int maxNum = 0, comVal;
    for (pair<int, int> it : vals)
        if (it.second > maxNum) {
            maxNum = it.second;
            comVal  = it.first;
        }

    cout << comVal;
    return 0;
}
