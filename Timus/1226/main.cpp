#include <iostream>
#include <string>
using namespace std;

int main()
{
    string line;
    while (getline(cin, line)) {
        int wordStart = -1;
        for (int i = 0; i <= (int)line.size(); ++i) {
            if (i == (int)line.size()) {
                if (wordStart != -1)
                    for (int j = i-1; j >= wordStart; --j)
                        cout << line[j];
                wordStart = -1;
                continue;
            }

            if ((line[i] >= 'a' && line[i] <= 'z') || (line[i] >= 'A' && line[i] <= 'Z')) {
                if (wordStart == -1)
                    wordStart = i;
            } else {
                if (wordStart != -1) {
                    for (int j = i-1; j >= wordStart; --j)
                        cout << line[j];
                    wordStart = -1;
                }
                cout << line[i];
            }
        }
        cout << "\n";
    }

    return 0;
}
