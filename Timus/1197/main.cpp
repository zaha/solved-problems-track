#include <iostream>
#include <string>
using namespace std;

int main() {
    int t;
    cin >> t;

    for (int i = 1; i <= t; ++i) {
        string pos;
        cin >> pos;

        if (pos[0] == 'a' || pos[0] == 'h') {
            if (pos[1] == '1' || pos[1] == '8') {
                cout << "2\n";
            } else if (pos[1] == '2' || pos[1] == '7') {
                cout << "3\n";
            } else {
                cout << "4\n";
            }
            
        } else if (pos[0] == 'b' || pos[0] == 'g') {
            if (pos[1] == '1' || pos[1] == '8') {
                cout << "3\n";
            }
            else if (pos[1] == '2' || pos[1] == '7') {
                cout << "4\n";
            } else {
                cout << "6\n";
            }
            
        } else {
            if (pos[1] == '1' || pos[1] == '8') {
                cout << "4\n";
            } else if (pos[1] == '2' || pos[1] == '7') {
                cout << "6\n";
            } else {
                cout << "8\n";
            }
        }
        
    }
}
