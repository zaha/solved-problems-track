#include <iostream>
using namespace std;

int main()
{
    int n;
    cin >> n;
    int a, b, c;
    a = b = c = 0;
    while (n--) {
        int temp;
        cin >> temp;
        if (temp == 1)
            ++a;
        else if (temp == 2)
            ++b;
        else
            ++c;
    }

    if (a && b && c)
        cout << "Yes";
    else if (!c) {
        if (a == 1 || b == 1) {
            if (a >= 5 || b >= 5)
                cout << "Yes";
            else
                cout << "No";
        } else if (a >= 2 && b >= 2)
            cout << "Yes";
        else
            cout << "No";
    } else if (!b) {
        if (a == 1 || c == 1) {
            if (a >= 5 || c >= 5)
                cout << "Yes";
            else
                cout << "No";
        } else if (a >= 2 && c >= 2)
            cout << "Yes";
        else
            cout << "No";
    } else if (!a) {
        if (c == 1 || b == 1) {
            if (c >= 5 || b >= 5)
                cout << "Yes";
            else
                cout << "No";
        } else if (c >= 2 && b >= 2)
            cout << "Yes";
        else
            cout << "No";
    }
    return 0;
}
