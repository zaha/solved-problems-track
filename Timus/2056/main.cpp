#include <iostream>

using namespace std;

int main()
{
    int n;
    cin >> n;

    //1 = none, 2 = common, 3 = high, 4 = personal
    int schoolarship = 3;
    int sum = 0;

    for (int i = 1; i <= n; ++i) {
        int temp;
        cin >> temp;
        sum += temp;

        if (temp == 3) {
            schoolarship = 1;
            break;
        }

    }

    if (schoolarship == 3)
        if (sum / n == 5)
            schoolarship = 4;
        else if ((double)sum / n < 4.5)
            schoolarship = 2;

    if (schoolarship == 1)
        cout << "None";
    else if (schoolarship == 2)
        cout << "Common";
    else if (schoolarship == 3)
        cout << "High";
    else
        cout << "Named";

    return 0;
}
