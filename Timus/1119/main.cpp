#include <iostream>
#include <vector>
#include <bitset>
#include <cmath>
using namespace std;

int n, m;
bitset <1003> diag[1003];
int maxDiags[10003][10003];

int main() {
    cin >> n >> m;

    int k;
    cin >> k;
    for (int i = 1; i <= k; ++i) {
        int a, b;
        cin >> a >> b;

        diag[a][b] = true;
    }

    for (int i = 1; i <= n + 1; ++i)
        for (int j = 1; j <= m + 1; ++j) {
            maxDiags[i][j] = max(maxDiags[i-1][j], maxDiags[i][j-1]);
            if (diag[i-1][j-1])
                maxDiags[i][j] = max(maxDiags[i][j], maxDiags[i-1][j-1] + 1);
        }

    cout << (n + m - maxDiags[n+1][m+1] * 2) * 100 + round(141.42 * maxDiags[n+1][m+1]);
    return 0;
}