#include <iostream>
#include <string>
using namespace std;

int main() {
    int n;
    string marks;
    cin >> n >> marks;
    
    int prod = 1;
    while (n > 0) {
        prod *= n;
        n -= marks.size();
    }

    cout << prod;
    return 0;
}