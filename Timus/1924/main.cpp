#include <iostream>
using namespace std;

int main() {
    int n, contor = 0;
    cin >> n;

    bool black = true;
    do {
        black = !black;
        contor += 2;
    } while (contor < n);

    if (black)
        cout << "black";
    else
        cout << "grimy";

    return 0;
}