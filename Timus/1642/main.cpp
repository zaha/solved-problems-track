#include <iostream>
using namespace std;

int main()
{
    int n, x;
    cin >> n >> x;

    int minPosObs = 1005, maxNegObs = -1005; // min positive obstacle, max negative obstacle
    while (n--) {
        int temp;
        cin >> temp;

        if (temp > 0 && temp < minPosObs)
            minPosObs = temp;
        if (temp < 0 && temp > maxNegObs)
            maxNegObs = temp;
    }

    if (maxNegObs > x || minPosObs < x) {
        cout << "Impossible";
        return 0;
    } else {
        if (x > 0)
            cout << x << " ";
        else
            cout << minPosObs*2 - x << " ";

        if (x < 0)
            cout << -x;
        else
            cout << -maxNegObs*2 + x;
    }

    return 0;
}
