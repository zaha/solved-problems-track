#include <iostream>
#include <queue>
using namespace std;

queue<int> nums;

int main() {
    int n;
    cin >> n;

    while  (n--) {
        int temp;
        cin >> temp;
        nums.emplace(temp);
    }

    bool daenerys = false;
    while (!nums.empty()) {
        while (!nums.empty() && nums.front() < 3)
            nums.pop();

        if (!nums.empty()) {
            nums.front() -= 2;
            daenerys = !daenerys;
        }
    }

    if (daenerys)
        cout << "Daenerys";
    else
        cout << "Stannis";
}