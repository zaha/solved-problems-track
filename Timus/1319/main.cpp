#include <iostream>

using namespace std;

int main()
{
    int n, mt[102][102], contor = 1;
    cin >> n;

    for (int j = n; j >= 1; --j) {
        int k = 1, l = j;
        while (l <= n) {
            ++k;
            ++l;
            mt[k][l] = contor;
            ++contor;
        }
    }

    for (int i = 2; i <= n; ++i) {
        int k = i, l = 1;
        while (k <= n) {
            ++k;
            ++l;
            mt[k][l] = contor;
            ++contor;
        }
    }

    for (int i = 2; i <= n + 1; ++i) {
        for (int j = 2; j <= n + 1; ++j)
            cout << mt[i][j] << " ";
        cout << "\n";
    }

    return 0;
}
