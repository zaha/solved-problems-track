#include <iostream>
#include <cmath>
#include <string>
using namespace std;

int main() {
    int n, pos = 1, steps = 0;
    cin >> n;
    for (int i = 1; i <= n; ++i) {
        string name;
        cin >> name;
        int target;
        if (name[0] == 'A' || name[0] == 'P' || name[0] == 'O' || name[0] == 'R')
            target = 1;
        else if (name[0] == 'B' || name[0] == 'M' || name[0] == 'S')
            target = 2;
        else
            target = 3;

        steps += abs(pos - target);
        pos = target;
    }

    cout << steps;
    return 0;
}
