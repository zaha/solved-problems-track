#include <iostream>
#include <set>
using namespace std;

int profDates[15005];
multiset <int> studDates;

int main() {
    int n;
    cin >> n;

    for (int i = 1; i <= n; ++i)
        cin >> profDates[i];

    int m;
    cin >> m;

    for (int i = 1; i <= m; ++i) {
        int temp;
        cin >> temp;
        studDates.insert(temp);
    }

    int counter = 0, i = 1;
    for (multiset <int> :: iterator it = studDates.begin(); it != studDates.end(); ++it) {
        int temp = *it;

        while (temp > profDates[i])
            ++i;
        
        if (temp == profDates[i])
            ++counter;
    }

    cout << counter;
    return 0;
}
