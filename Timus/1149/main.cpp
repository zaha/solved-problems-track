#include <iostream>
using namespace std;

void calcA(int n) {
    for (int i = 1; i < n; ++i) {
        cout << "sin(" << i;
        if (i & 1)
            cout << "-";
        else
            cout << "+";
    }

    cout << "sin(" << n;

    for (int i = 1; i <= n; ++i)
        cout << ")";
}

void calcS(int n) {
    for (int i = 1; i < n; ++i)
        cout << "(";

    for (int j = 1; j < n; ++j) {
        calcA(j);
        cout << "+" << n-j+1;
        cout << ")";
    }
    
    calcA(n);
    cout << "+1";
}

int main() {
    int n;
    cin >> n;

    calcS(n);
    return 0;
}