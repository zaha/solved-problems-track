#include <bits/stdc++.h>
using namespace std;
#define up true
#define down false
typedef tuple<int, int, bool> triplePair; //destination, cost, mode
const int N = 1e5 + 7;

int start, stop, n, m;
vector<pair<int, bool>> graph[N]; // where, how
bitset<N> visited;

struct calcNext {
    bool operator()(triplePair x, triplePair y) const {
        return get<1>(x) > get<1>(y);
    }
};
priority_queue<triplePair, vector<triplePair>, calcNext> q;

void addChilds(triplePair node) {
    visited[get<0>(node)] = true;
    for (pair<int, bool> child : graph[get<0>(node)])
        if (!visited[child.first])
            q.emplace(make_tuple(child.first, get<1>(node) + (child.second != get<2>(node)), child.second));
}

int solve() {
    addChilds(make_tuple(start, 0, up));
    addChilds(make_tuple(start, 0, down));

    while (true) {
        // clean q
        while (visited[get<0>(q.top())])
            q.pop();

        if (get<0>(q.top()) == stop)
            return get<1>(q.top());
        addChilds(q.top());
    }
}

int main() {
    cin >> n >> m;
    for (int i = 1; i <= m; ++i) {
        int x, y;
        cin >> x >> y;
        graph[x].emplace_back(make_pair(y, up));
        graph[y].emplace_back(make_pair(x, down));
    }
    cin >> start >> stop;

    cout << solve();
    return 0;
}
