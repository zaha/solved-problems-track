#include <iostream>
#include <string>
using namespace std;

int main() {
    int n, total = 2;
    cin >> n;

    for (int i = 1; i <= n; ++i) {
        string temp;
        cin >> temp;
        if (temp[temp.size() - 4] == '+')
            total += 2;
        else
            ++total;
    }

    if (total == 13)
        ++total;

    cout << total * 100;
    return 0;
}
