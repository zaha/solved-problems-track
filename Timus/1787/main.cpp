#include <iostream>
using namespace std;

int main()
{
    int k, n, jam = 0;
    cin >> k >> n;

    for (int i = 1; i <= n; ++i) {
        int temp;
        cin >> temp;
        if (temp > k)
            jam += temp - k;
        else
            jam -= k - temp;

        if (jam < 0)
            jam = 0;
    }

    cout << jam;
    return 0;
}
